/**
 * pygnt- Python bindings for the GNT toolkit.
 * Copyright (C) 2007 Sadrul Habib Chowdhury <sadrul@pidgin.im>
 *
 *   gnttree.override: overrides for the tree widget.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301
 * USA
 */
%%
headers
#include "common.h"
%%
ignore 
gnt_tree_create_row
gnt_tree_create_row_from_list
gnt_tree_new_with_columns
gnt_tree_add_row_last
%%
override gnt_tree_new
static void
deref_key(gpointer data)
{
	Py_DECREF((PyGObject*)data);
}

static int
_wrap_gnt_tree_new(PyGObject *self, PyObject *args, PyObject *kwargs)
{
	static char* kwlist[] = { NULL };

	if (!PyArg_ParseTupleAndKeywords(args, kwargs,
				":gnt.Tree.__init__",
				kwlist))
		return -1;

	pygobject_constructv(self, 0, NULL);
	if (!self->obj) {
		PyErr_SetString(
				PyExc_RuntimeError, 
				"could not create gnt.Tree object");
		return -1;
	}
	g_object_set(G_OBJECT(self->obj), "columns", 1, NULL);
	gnt_tree_set_hash_fns(GNT_TREE(self->obj), g_direct_hash, g_direct_equal, deref_key);
	return 0;
}
%%
override gnt_tree_get_selection_text_list noargs
static PyObject *
_wrap_gnt_tree_get_selection_text_list(PyGObject *self)
{
	GList *list = gnt_tree_get_selection_text_list(GNT_TREE(self->obj));
	return create_pyobject_from_string_list(list);
}
%%
override gnt_tree_get_rows noargs
static PyObject *
_wrap_gnt_tree_get_rows(PyGObject *self)
{
	GList *list = gnt_tree_get_rows(GNT_TREE(self->obj));
	PyObject *py_list;
	if (list == NULL) {
		Py_INCREF(Py_None);
		return Py_None;
	}
	if ((py_list = PyList_New(0)) == NULL) {
		return NULL;
	}
	while (list) {
		PyObject *obj = list->data;
		PyList_Append(py_list, obj);
		list = list->next;
	}
	return py_list;
}
%%
override gnt_tree_add_row_after
static PyObject *
_wrap_gnt_tree_add_row_after(PyGObject *self, PyObject *args)
{
	static char *kwlist[] = {"key", "row", "parent", "bigbro", NULL};
	PyObject *py_list;
	gpointer key, parent, bigbro = NULL;
	int len, i;
	GList *list = NULL;
	GntTreeRow *row;
	gboolean insert_last = FALSE;
	int choice = FALSE;

	if (!PyArg_ParseTuple(args,
				"O!OO|Oi:GntTree.add_row_after",
				&PyGObject_Type, &key,
				&py_list,
				&parent,
				&bigbro, &choice))
		return NULL;

	len = PySequence_Length(py_list);
	for (i = 0; i < len; i++) {
		PyObject *item = PySequence_GetItem(py_list, i);
		if (!pygobject_check(item, &PyString_Type)) {
			PyErr_SetString(PyExc_TypeError,
					"column_list members must be strings");
			Py_DECREF(item);
			return NULL;
		}
		list = g_list_prepend(list, PyString_AsString(item));
		Py_DECREF(item);
	}

	if (parent == Py_None)
		parent = NULL;
	if (bigbro == Py_None)
		bigbro = NULL;
	else if (bigbro == NULL)
		insert_last = TRUE;

	list = g_list_reverse(list);
	row = gnt_tree_create_row_from_list(GNT_TREE(self->obj), list);
	if (insert_last) {
		gnt_tree_add_row_last(GNT_TREE(self->obj),
				key, row, parent);
	} else {
		if (choice)
			gnt_tree_add_choice(GNT_TREE(self->obj), key, row, parent, bigbro);
		else
			gnt_tree_add_row_after(GNT_TREE(self->obj),
					key, row,
					parent, bigbro);
	}
	Py_INCREF((PyGObject*)key);
	g_list_free(list);

	Py_INCREF(Py_None);
	return Py_None;
}
%%
override gnt_tree_get_selection_data noargs
static PyObject *
_wrap_gnt_tree_get_selection_data(PyGObject *self)
{
	PyObject *ret = gnt_tree_get_selection_data(GNT_TREE(self->obj));
	if (!ret)
		ret = Py_None;
	Py_INCREF(ret);
	return ret;
}
%%
override gnt_tree_set_choice kwargs
static PyObject *
_wrap_gnt_tree_set_choice(PyGObject *self, PyObject *args, PyObject *kwargs)
{
	gpointer key;
	int choice;
	if (!PyArg_ParseTuple(args, "O!i:GntTree.set_choice", &PyGObject_Type, &key, &choice)) {
		return NULL;
	}
	gnt_tree_set_choice(GNT_TREE(self->obj), key, choice);
	Py_INCREF(Py_None);
	return Py_None;
}
%%
override gnt_tree_get_choice kwargs
static PyObject *
_wrap_gnt_tree_get_choice(PyGObject *self, PyObject *args, PyObject *kwargs)
{
	gpointer key;
	gboolean choice;
	if (!PyArg_ParseTuple(args, "O!:GntTree.get_choice", &PyGObject_Type, &key)) {
		return NULL;
	}
	choice = gnt_tree_get_choice(GNT_TREE(self->obj), key);
	return PyBool_FromLong(choice);
}
%%
override gnt_tree_change_text kwargs
static PyObject *
_wrap_gnt_tree_change_text(PyGObject *self, PyObject *args, PyObject *kwargs)
{
	static char *kwlist[] = { "key", "colno", "text", NULL };
	char *text;
	int colno;
	gpointer key;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs,"O!is:GntTree.change_text", kwlist, &PyGObject_Type, &key, &colno, &text))
		return NULL;

	gnt_tree_change_text(GNT_TREE(self->obj), key, colno, text);

	Py_INCREF(Py_None);
	return Py_None;
}
%%
override gnt_tree_set_row_color kwargs
static PyObject *
_wrap_gnt_tree_set_row_color(PyGObject *self, PyObject *args, PyObject *kwargs)
{
	static char *kwlist[] = { "key", "color", NULL };
	gpointer key;
	int color;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs,"O!i:GntTree.set_row_color", kwlist, &PyGObject_Type, &key, &color))
		return NULL;

	gnt_tree_set_row_color(GNT_TREE(self->obj), key, color);

	Py_INCREF(Py_None);
	return Py_None;
}
%%
override gnt_tree_set_row_flags kwargs
static PyObject *
_wrap_gnt_tree_set_row_flags(PyGObject *self, PyObject *args, PyObject *kwargs)
{
	static char *kwlist[] = { "key", "flag", NULL };
	int flag;
	gpointer key;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs,"O!i:GntTree.set_row_flags", kwlist, &PyGObject_Type, &key, &flag))
		return NULL;

	gnt_tree_set_row_flags(GNT_TREE(self->obj), key, flag);

	Py_INCREF(Py_None);
	return Py_None;
}
%%
override gnt_tree_remove kwargs
static PyObject *
_wrap_gnt_tree_remove(PyGObject *self, PyObject *args, PyObject *kwargs)
{
	static char *kwlist[] = { "key", NULL };
	gpointer key;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs,"O!:GntTree.remove", kwlist, &PyGObject_Type, &key))
		return NULL;

	gnt_tree_remove(GNT_TREE(self->obj), key);

	Py_INCREF(Py_None);
	return Py_None;
}
%%
override gnt_tree_sort_siblings kwargs
static PyObject *
_wrap_gnt_tree_sort_siblings(PyGObject *self, PyObject *args, PyObject *kwargs)
{
	static char *kwlist[] = { "key", NULL };
	gpointer key;

	if (!PyArg_ParseTupleAndKeywords(args, kwargs,"O!:GntTree.sort_siblings", kwlist, &PyGObject_Type, &key))
		return NULL;

	gnt_tree_sort_siblings(GNT_TREE(self->obj), key);

	Py_INCREF(Py_None);
	return Py_None;
}
%%
override gnt_tree_set_selected
static PyObject *
_wrap_gnt_tree_set_selected(PyGObject *self, PyObject *args)
{
	gpointer key;
	if (!PyArg_ParseTuple(args, "O!:GntTree.set_selected", &PyGObject_Type, &key)) {
		return NULL;
	}
	gnt_tree_set_selected(GNT_TREE(self->obj), key);
	Py_INCREF(Py_None);
	return Py_None;
}
%%
override gnt_tree_set_expanded
static PyObject *
_wrap_gnt_tree_set_expanded(PyGObject *self, PyObject *args)
{
	gpointer key;
	int expanded;
	if (!PyArg_ParseTuple(args, "O!i:GntTree.set_expanded", &PyGObject_Type, &key, &expanded)) {
		return NULL;
	}
	gnt_tree_set_expanded(GNT_TREE(self->obj), key, expanded);
	Py_INCREF(Py_None);
	return Py_None;
}
%%
ignore gnt_tree_set_compare_func
%%
define GntTree.enable_sort noargs

/**
  Both p1 and p2 are expected to be GObjects, with a 'compare_func' property.
 */
static int
custom_compare_func(gconstpointer p1, gconstpointer p2)
{
	PyObject *compare_func;
	PyObject *ret, *params;
	const PyGObject *o1, *o2;
	int retval;
	PyTypeObject *class;

	o1 = p1;
	o2 = p2;

	g_return_val_if_fail(G_IS_OBJECT(o1->obj), -1);
	g_return_val_if_fail(G_IS_OBJECT(o2->obj), -1);

	class = pygobject_lookup_class(G_TYPE_FROM_INSTANCE(o1->obj));
	compare_func = PyDict_GetItemString(class->tp_dict, "COMPARE_FUNC");
	g_return_val_if_fail(compare_func && PyCallable_Check(compare_func), -1);

	params = PyTuple_New(2);
	Py_INCREF((PyObject*)o1);
	PyTuple_SetItem(params, 0, (PyObject*)o1);
	Py_INCREF((PyObject*)o2);
	PyTuple_SetItem(params, 1, (PyObject*)o2);
	ret = PyObject_CallObject(compare_func, params);
	retval = (int)PyInt_AsLong(ret);

	Py_DECREF(params);
	Py_DECREF(ret);
	return retval;
}

static PyObject *
_wrap_gnt_tree_enable_sort(PyGObject *self)
{
	gnt_tree_set_compare_func(GNT_TREE(self->obj), custom_compare_func);

	Py_INCREF(Py_None);
	return Py_None;
}

