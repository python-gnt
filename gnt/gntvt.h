#ifndef GNT_VT_H
#define GNT_VT_H

#include <rote/rote.h>
#include "gntwindow.h"
#include "gnt.h"
#include "gntcolors.h"
#include "gntkeys.h"

#define GNT_TYPE_VT				(gnt_vt_get_gtype())
#define GNT_VT(obj)				(G_TYPE_CHECK_INSTANCE_CAST((obj), GNT_TYPE_VT, GntVT))
#define GNT_VT_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), GNT_TYPE_VT, GntVTClass))
#define GNT_IS_VT(obj)			(G_TYPE_CHECK_INSTANCE_TYPE((obj), GNT_TYPE_VT))
#define GNT_IS_VT_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GNT_TYPE_VT))
#define GNT_VT_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GNT_TYPE_VT, GntVTClass))

#define GNT_VT_FLAGS(obj)				(GNT_VT(obj)->priv.flags)
#define GNT_VT_SET_FLAGS(obj, flags)		(GNT_VT_FLAGS(obj) |= flags)
#define GNT_VT_UNSET_FLAGS(obj, flags)	(GNT_VT_FLAGS(obj) &= ~(flags))

typedef struct _GnVT			GntVT;
typedef struct _GnVTPriv		GntVTPriv;
typedef struct _GnVTClass		GntVTClass;

struct _GnVT
{
	GntWindow parent;
	RoteTerm *vt;
	int timeout;
};

struct _GnVTClass
{
	GntWindowClass parent;

	void (*gnt_reserved1)(void);
	void (*gnt_reserved2)(void);
	void (*gnt_reserved3)(void);
	void (*gnt_reserved4)(void);
};

G_BEGIN_DECLS

GType gnt_vt_get_gtype(void);

GntWidget *gnt_vt_new();

G_END_DECLS

#endif /* GNT_VT_H */
