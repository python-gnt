#ifndef GNT_ACTION_H
#define GNT_ACTION_H

#include <glib.h>

typedef gboolean (*GntWidgetActionCallback) (GntWidget *widget, GList *params);
typedef gboolean (*GntWidgetActionCallbackNoParam)(GntWidget *widget);

typedef struct _GnWidgetAction GntWidgetAction;
typedef struct _GnWidgetActionParam GntWidgetActionParam;

struct _GnWidgetAction
{
	char *name;        /* The name of the action */
	union {
		gboolean (*action)(GntWidget *widget, GList *params);
		gboolean (*action_noparam)(GntWidget *widget);
	} u;
};

struct _GnWidgetActionParam
{
	GntWidgetAction *action;
	GList *list;
};


GntWidgetAction *gnt_widget_action_parse(const char *name);

void gnt_widget_action_free(GntWidgetAction *action);
void gnt_widget_action_param_free(GntWidgetActionParam *param);

#endif
