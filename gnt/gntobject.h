#ifndef GNT_OBJECT_H
#define GNT_OBJECT_H

#include <stdio.h>
#include <glib.h>
#include <glib-object.h>
#include <ncurses.h>

#define GNT_TYPE_OBJECT				(gnt_object_get_gtype())
#define GNT_OBJECT(obj)				(G_TYPE_CHECK_INSTANCE_CAST((obj), GNT_TYPE_OBJECT, GntObject))
#define GNT_OBJECT_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST((klass), GNT_TYPE_OBJECT, GntObjectClass))
#define GNT_IS_OBJECT(obj)			(G_TYPE_CHECK_INSTANCE_TYPE((obj), GNT_TYPE_OBJECT))
#define GNT_IS_OBJECT_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE((klass), GNT_TYPE_OBJECT))
#define GNT_OBJECT_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS((obj), GNT_TYPE_OBJECT, GntObjectClass))

#define	GNTDEBUG	fprintf(stderr, "%s\n", __FUNCTION__)

typedef struct _GnObject			GntObject;
typedef struct _GnObjectClass		GntObjectClass;

struct _GnObject
{
	GObject inherit;
};

struct _GnObjectClass
{
	GObjectClass parent;

	GHashTable *remaps;   /* Key remaps */
	GHashTable *actions;  /* name -> Action */
	GHashTable *bindings; /* key -> ActionParam */

	void (*gnt_reserved1)(void);
	void (*gnt_reserved2)(void);
	void (*gnt_reserved3)(void);
	void (*gnt_reserved4)(void);
};

G_BEGIN_DECLS

GType gnt_object_get_gtype(void);

/******************/
/*   Key Remaps   */
/******************/
const char * gnt_object_remap_keys(GntObject *object, const char *text);

/******************/
/* Object Actions */
/******************/
typedef gboolean (*GntObjectActionCallback) (GntObject *object, GList *params);
typedef gboolean (*GntObjectActionCallbackNoParam)(GntObject *object);

typedef struct _GnObjectAction GntObjectAction;
typedef struct _GnObjectActionParam GntObjectActionParam;

struct _GnObjectAction
{
	char *name;        /* The name of the action */
	union {
		gboolean (*action)(GntObject *object, GList *params);
		gboolean (*action_noparam)(GntObject *object);
	} u;
};

struct _GnObjectActionParam
{
	GntObjectAction *action;
	GList *list;
};


/*GntObjectAction *gnt_object_action_parse(const char *name);*/

void gnt_object_action_free(GntObjectAction *action);
void gnt_object_action_param_free(GntObjectActionParam *param);

void gnt_object_class_register_action(GntObjectClass *klass, const char *name,
			GntObjectActionCallback callback, const char *trigger, ...);
void gnt_object_register_binding(GntObjectClass *klass, const char *name,
			const char *trigger, ...);

gboolean gnt_object_perform_action_key(GntObject *object, const char *keys);
gboolean gnt_object_perform_action_named(GntObject *object, const char *name, ...);

G_END_DECLS

#endif /* GNT_OBJECT_H */
