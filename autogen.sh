#!/bin/sh

aclocal 
autoheader 
automake --add-missing --copy;
autoconf
automake

echo;
echo "Running ./configure $@"
echo;
./configure $@

