#!/usr/bin/env python

"""Buddylist for any purple client"""

import gnt
import gobject

class Row(gobject.GObject):
	def __init__(self, node):
		self.__gobject_init__()
		self.node = node
gobject.type_register(Row)

def parent_node(purple, node):
	if purple.PurpleBlistNodeIsBuddy(node):
		return purple.PurpleBuddyGetContact(node)
	if purple.PurpleBlistNodeIsChat(node):
		return purple.PurpleChatGetGroup(node)
	if purple.PurpleBlistNodeIsContact(node):
		return purple.PurpleBuddyGetGroup(purple.PurpleContactGetPriorityBuddy(node))
	return None

def display_name(purple, node):
	if purple.PurpleBlistNodeIsBuddy(node):
		return purple.PurpleBuddyGetName(node)
	if purple.PurpleBlistNodeIsChat(node):
		return purple.PurpleChatGetName(node)
	if purple.PurpleBlistNodeIsGroup(node):
		return purple.PurpleGroupGetName(node)
	if purple.PurpleBlistNodeIsContact(node):
		return purple.PurpleBuddyGetName(purple.PurpleContactGetPriorityBuddy(node))
	return "..."

def is_visible(purple, node):
	if purple.PurpleBlistNodeIsBuddy(node):
		if not purple.PurpleAccountIsConnected(purple.PurpleBuddyGetAccount(node)):
			return False
		return purple.PurplePresenceIsOnline(purple.PurpleBuddyGetPresence(node))
	if purple.PurpleBlistNodeIsChat(node):
		return True #purple.PurpleAccountIsConnected(purple.PurpleChatGetAccount(node))
	if purple.PurpleBlistNodeIsGroup(node):
		return (purple.PurpleBlistGetGroupOnlineCount(node) > 0)
	if purple.PurpleBlistNodeIsContact(node):
		return is_visible(purple, purple.PurpleContactGetPriorityBuddy(node))
	return False

class BuddyList(gnt.Tree):
	def __init__(self, purple, bus):
		gnt.Tree.__init__(self)
		self.purple = purple
		self.bus = bus
		bus.add_signal_receiver(self.buddysignedon,
                        dbus_interface = "im.pidgin.purple.PurpleInterface",
                        signal_name = "BuddySignedOn")
		bus.add_signal_receiver(self.buddysignedoff,
                        dbus_interface = "im.pidgin.purple.PurpleInterface",
                        signal_name = "BuddySignedOff")
		self.rows = {}

		node = purple.PurpleBlistGetRoot()
		while node:
			self.ensure_row(node)
			node = purple.PurpleBlistNodeNext(node, False)

	def ensure_row(self, node):
		if node not in self.rows:
			if not is_visible(self.purple, node):	return None
			row = Row(node)
			self.rows[node] = row
			parent = parent_node(self.purple, node)
			prow = None
			if parent:
				prow = self.ensure_row(parent)
			self.add_row_after(row, [str(display_name(self.purple, node))], prow, None)
			if self.purple.PurpleBlistNodeIsContact(node):
				self.set_expanded(row, False)
		return self.rows[node]

	def update_node(self, node):
		row = self.ensure_row(node)

	def buddysignedon(self, buddy):
		self.update_node(buddy)

	def remove_node(self, node):
		if node not in self.rows:
			return
		self.remove(self.rows[node])
		del self.rows[node]
		parent = parent_node(self.purple, node)
		if not parent:	return
		if not is_visible(self.purple, parent):
			self.remove_node(parent)

	def buddysignedoff(self, buddy):
		self.remove_node(buddy)

gobject.type_register(BuddyList)
gnt.register_bindings(BuddyList)

