#!/usr/bin/env python

"""
gntid3 - An ID3 editor for MP3 songs, using libgnt and id3.

Copyright (C) 2007 Sadrul Habib Chowdhury <sadrul@pidgin.im>

This application is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this application; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
USA
"""

import sys
import os
import gnt

__version__ = "0.0.1alpha"
__author__ = "Sadrul Habib Chowdhury (sadrul@pidgin.im)"
__copyright__ = "Copyright 2007, Sadrul Habib Chowdhury"
__license__ = "GPL" # see full license statement above

base = "/audio/incoming/"
template = "{artist}/{album}/{artist} - {title}{ext}"

def safe(id3, attr):
	try:
		return id3[attr]
	except:
		return ""

def update_template(ignore, gi):
	name = template
	name = name.replace("{artist}", gi.artist.get_text())
	name = name.replace("{album}", gi.album.get_text())
	name = name.replace("{title}", gi.title.get_text())
	name = name.replace("{year}", gi.year.get_text())
	name = name.replace("{ext}", os.path.splitext(gi.filename)[1])
	name = base + name
	gi.newname.set_text(name)

def pack_widget(label, entry, gi):
	box = gnt.Box(homo = False, vert = False)
	box.add_widget(gnt.Label(label))
	box.add_widget(entry)
	entry.connect("text_changed", update_template, gi)
	return box

class GntID3:
	def __init__(self, filename):
		self.filename = filename
		self.win = None
		self.id3 = None

	def show(self):
		self.win = win = gnt.Box(homo = False, vert = True)
		win.set_toplevel(True)
		win.set_title("GntID3")
		win.set_alignment(gnt.ALIGN_MID)
		win.set_pad(0)
		if self.filename:
			self.id3 = ID3.ID3(self.filename)

		if self.filename:
			filelabel = gnt.Label("File: " + os.path.basename(self.filename))
			pathlabel = gnt.Label("Path: " + os.path.dirname(self.filename))
		else:
			filelabel = gnt.Label("File: ")
			pathlabel = gnt.Label("Path: ")

		sel = gnt.Button("Select File...")
		win.add_widget(filelabel)
		win.add_widget(pathlabel)
		win.add_widget(pack_box(False, False, [sel]))
		win.add_widget(gnt.Line(False))

		def file_selected(filesel, path, filename):
			file = path
			self.filename = file
			self.id3 = ID3.ID3(file)
			reset_fields(reset)
			filesel.destroy()
			filelabel.set_text("File: " + os.path.basename(file))
			pathlabel.set_text("Path: " + os.path.dirname(file))
		def select_file(button):
			filesel = gnt.FileSel()
			filesel.set_title("Select Song")
			filesel.set_multi_select(False)
			filesel.connect('file_selected', file_selected)
			filesel.show()
			def reset_filesel(fl):
				global filesel
				filesel = None
			filesel.connect('destroy', reset_filesel)
		sel.connect('activate', select_file)

		self.title = title = gnt.Entry(safe(self.id3, 'TITLE'))
		win.add_widget(pack_widget("Title", title, self))

		self.artist = artist = gnt.Entry(safe(self.id3, 'ARTIST'))
		win.add_widget(pack_widget("Artist", artist, self))

		self.album = album = gnt.Entry(safe(self.id3, 'ALBUM'))
		win.add_widget(pack_widget("Album", album, self))

		self.year = year = gnt.Entry(safe(self.id3, 'YEAR'))
		win.add_widget(pack_widget("Year", year, self))

		self.comment = comment = gnt.Entry(safe(self.id3, 'COMMENT'))
		win.add_widget(pack_widget("Comment", comment, self))

		fields = [['TITLE', 'Title', title],
				  ['ARTIST', 'Artist', artist],
				  ['ALBUM', 'Album', album],
				  ['YEAR', 'Year', year],
				  ['COMMENT', 'comment', comment],
				  ]

		box = gnt.Box(homo = False, vert = False)
		save = gnt.Button("Save")
		box.add_widget(save)
		reset = gnt.Button("Reset")
		box.add_widget(reset)
		clear = gnt.Button("Clear All")
		box.add_widget(clear)

		win.add_widget(box)

		def clear_fields(button):
			for field in fields:
				field[2].set_text("")
		clear.connect('activate', clear_fields)

		def save_fields(button):
			for field in fields:
				value = field[2].get_text()
				self.id3[field[0]] = value
			self.id3.write()
		save.connect('activate', save_fields)

		def reset_fields(button):
			for field in fields:
				value = safe(self.id3, field[0])
				field[2].set_text(value)
		reset.connect('activate', reset_fields)

		self.newname = newname = gnt.Entry("")
		win.add_widget(newname)

		self.win.show()

###

def pack_box(homo, vert, widgets):
	box = gnt.Box(homo = homo, vert = vert)
	for widget in widgets:
		box.add_widget(widget)
	return box

def exit_with_error(message):
	win.add_widget(gnt.Label(message))
	button = gnt.Button("OK")
	win.add_widget(pack_box(False, False, [button]))
	def end_prog(button):
		gnt.gnt_quit()
		sys.exit(1)
	button.connect('activate', end_prog)
	win.show()
	gnt.gnt_main()

try:
	import ID3
except:
	exit_with_error("You need package 'ID3' for python.")

file = None
if len(sys.argv) >= 2:
	file = sys.argv[1]

info = GntID3(file)
info.show()

gnt.gnt_main()

gnt.gnt_quit()

