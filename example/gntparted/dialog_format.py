#!/usr/bin/env python

"""The 'Format Partition' dialog.
"""

import gobject
import gnt
import parted
import common
import sys

import gppart
import gpdevice

class FormatDialog:
	def __init__(self):
		self.reset()

	def reset(self):
		self.window = None
		self.device = None
		self.fstype = None
		self.partition = None
		self.mwindow = None

	def set_dev_part(self, dev, part, win):
		self.device = dev
		self.partition = part
		self.mwindow = win

	def show(self):
		if self.window:
			self.window.destroy()
		self.window = win = gnt.Box(homo = False, vert = True)
		
		win.set_toplevel(True)
		win.set_title("Format Partition")
		win.set_pad(0)

		# Add widgets here
		combo = self.fstype = gnt.ComboBox()
		for type in common.filesystems:
			combo.add_data(type, type)

		win.add_widget(common.create_box(gnt.Label("Filesystem: "), self.fstype, True))

		def format_partition(button):
			fstype = self.fstype.get_selected_data()
			fstype = parted.file_system_type_get(fstype)
			fs = parted.FileSystem(self.partition.part.get_geom(), fstype)
			fs.close()
			self.partition.part.set_fs_type(fstype)
			#self.device.disk.write()
			self.mwindow.update_partition_list(None)
			self.window.destroy()
		button = gnt.Button("Format")
		button.connect('activate', format_partition)

		def cancel_dialog(button):
			self.window.destroy()
		cancel = gnt.Button("Cancel")
		cancel.connect('activate', cancel_dialog)

		win.add_widget(common.create_box(button, cancel))

		def destroy_cb(win):
			self.reset()
		win.connect('destroy', destroy_cb)
		win.set_alignment(gnt.ALIGN_MID)
		win.show()

formatdialog = FormatDialog()

def show_format_dialog(device, partition, mwindow):
	formatdialog.set_dev_part(device, partition, mwindow)
	formatdialog.show()

