#!/usr/bin/env python

"""Some common utility functions.
"""

import gnt
import re
import parted

def convert_size(size):
	"""Convert a string like '500MB' into size that can be used to create partitions etc.
	"""
	ret = 0
	mb = re.compile("^([0-9]+)\s*(MB?)?$")
	match = mb.match(size)
	if match:
		ret = long(match.group(1)) * (1 << 20) / parted.SECTOR_SIZE
		return ret

	gkb = re.compile("^([0-9]+)\s*(G|K)B?$")
	match = gkb.match(size)
	if match:
		unit = 1 << 10
		if match.group(2) == 'G':
			unit = 1 << 30
		ret = long(match.group(1)) * unit / parted.SECTOR_SIZE
		return ret
	return ret

def get_display_size(size):
	"""Return a good display string for size (in bytes).
	"""
	unit = "b"
	if size > (1 << 30):
		size = float(size / (1 << 30))
		unit = "G"
	elif size > (1 << 20):
		size = float(size / (1 << 20))
		unit = "M"
	elif size > (1 << 10):
		size = float(size / (1 << 10))
		uni = "K"
	size = ("%.1lf " + unit) % (float(size))
	return size

def create_box(label, widget, homo = False, vert = False):
	hbox = gnt.Box(homo = homo, vert = vert)
	hbox.add_widget(label)
	hbox.add_widget(widget)
	hbox.set_fill(False)
	hbox.set_alignment(gnt.ALIGN_MID)
	return hbox

filesystems = ['ext2', 'fat32', 'fat16', 'HFS', 'linux-swap', 'NTFS', 'reiserfs', 'ufs']
