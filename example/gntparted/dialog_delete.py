#!/usr/bin/env python

"""The 'Delete Partition' dialog.
"""

import gobject
import gnt
import parted
import common
import sys

import gppart
import gpdevice

class DeleteDialog:
	def __init__(self):
		self.reset()

	def reset(self):
		self.window = None
		self.device = None
		self.partition = None
		self.mwindow = None

	def set_dev_part(self, dev, part, win):
		self.device = dev
		self.partition = part
		self.mwindow = win
		if self.partition.part is None or self.partition.part.get_num() < 0:
			self.reset()
			return False
		return True

	def show(self):
		if self.window:
			self.window.destroy()
		self.window = win = gnt.Box(homo = False, vert = True)
		
		win.set_toplevel(True)
		win.set_title("Delete Partition")
		win.set_pad(0)

		win.add_widget(gnt.Label("Are you absolutely sure you want to delete \
partition " + self.device.device.get_path() + str(self.partition.part.get_num()) + "?\n\
Deleting the partition will cause you to lose the data in it."))
	
		def delete_cb(win):
			self.device.disk.delete_partition(self.partition.part)
			self.device.disk.write()
			self.mwindow.update_partition_list(None)
			self.window.destroy()
			pass
		delete = gnt.Button("Delete")
		delete .connect('activate', delete_cb)

		def cancel_dialog(button):
			self.window.destroy()
		cancel = gnt.Button("Cancel")
		cancel.connect('activate', cancel_dialog)

		win.add_widget(common.create_box(delete, cancel))

		def destroy_cb(win):
			self.reset()
		win.connect('destroy', destroy_cb)
		win.set_alignment(gnt.ALIGN_MID)
		win.show()

deletedialog = DeleteDialog()
def show_delete_dialog(device, partition, mwindow):
	if not deletedialog.set_dev_part(device, partition, mwindow):
		return
	deletedialog.show()


