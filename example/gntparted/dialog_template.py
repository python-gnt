#!/usr/bin/env python

"""The '' dialog.
"""

import gobject
import gnt
import parted
import common
import sys

import gppart
import gpdevice

class Dialog:
	def __init__(self):
		self.reset()

	def reset(self):
		self.window = None
		self.device = None
		self.partition = None
		self.mwindow = None

	def set_dev_part(self, dev, part, win):
		self.device = dev
		self.partition = part
		self.mwindow = win

	def show(self):
		if self.window:
			self.window.destroy()
		self.window = win = gnt.Box(homo = False, vert = True)
		
		win.set_toplevel(True)
		win.set_title("")
		win.set_pad(0)

		# Add widgets here
		
		def destroy_cb(win):
			self.reset()
		win.connect('destroy', destroy_cb)
		win.set_alignment(gnt.ALIGN_MID)
		win.show()

dialog = Dialog()
def show_dialog(device, partition, mwindow):
	dialog.set_dev_part(device, partition, mwindow)
	dialog.show()

