#!/usr/bin/env python

"""The 'Label Disk' dialog.
"""

import gobject
import gnt
import parted
import common
import gpdevice
import sys

class LabelDialog:
	def __init__(self):
		self.reset()

	def reset(self):
		self.window = None
		self.label = None
		self.device = None
		self.partition = None
		self.mwindow = None

	def set_dev_part(self, dev, part, win):
		self.device = dev
		self.partition = part
		self.mwindow = win

	def show(self):
		if self.window:
			self.window.destroy()
		self.window = win = gnt.Box(homo = False, vert = True)
		
		win.set_toplevel(True)
		win.set_title("Label Disk")
		win.set_pad(1)

		entry = self.label = gnt.Entry('')
		for type in ['bsd', 'loop', 'gpt', 'mac', 'msdos', 'pc98', 'sun']:
			entry.add_suggest(type)

		win.add_widget(common.create_box(gnt.Label("Disk Label for " + self.device.device.get_path() + ": "), self.label, True))

		def create_label(button):
			device = self.device.device
			label = self.label.get_text()
			if label is None or len(label) == 0:
				return
			type = parted.disk_type_get(label)
			if type is None or len(str(type.get_name())) <= 1:
				return
			disk = device.disk_create(type)
			disk.write()
			disk.close()
			self.mwindow.update_device_list(None)
			self.mwindow.update_partition_list(None)
			self.window.destroy()

		create = gnt.Button("Create Label")
		create.connect('activate', create_label)

		def cancel_dialog(button):
			self.window.destroy()
		cancel = gnt.Button("Cancel")
		cancel.connect('activate', cancel_dialog)

		win.add_widget(common.create_box(create, cancel))

		def destroy_cb(win, dlg):
			dlg.reset()
		win.connect('destroy', destroy_cb, self)
		win.set_alignment(gnt.ALIGN_MID)
		win.show()

labeldialog = LabelDialog()
def show_label_dialog(device, partition, mwindow):
	labeldialog.set_dev_part(device, partition, mwindow)
	labeldialog.show()

