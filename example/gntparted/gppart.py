#!/usr/bin/env python

"""Partition object.
"""

import gobject
import gnt
import parted
import common

def get_partition_type_name(type):
	return {parted.PARTITION_PRIMARY: 'primary',
	parted.PARTITION_LOGICAL: 'logical',
	parted.PARTITION_EXTENDED: 'extended',
	parted.PARTITION_FREESPACE: 'free',
	parted.PARTITION_METADATA: 'meta'}[type]

class Partition(gobject.GObject):
	def __init__(self, part):
		self.__gobject_init__()
		self.part = part
		self.fs = self.part.get_fs_type()

	def __del__(self):
		pass

	def get_bytes(self, num):
		return num * self.part.disk.get_dev().get_sector_size()

	def get_size(self):
		return float(self.get_bytes(self.part.get_geom().get_length()))

	def create_row(self):
		if self.fs:
			fs = self.fs.get_name()
		else:
			fs = get_partition_type_name(self.part.get_type())
		flag = ""
		if self.part.get_bootable():
			flag = "*"
		start = common.get_display_size(self.get_bytes(self.part.get_geom().get_start()))
		end = common.get_display_size(self.get_bytes(self.part.get_geom().get_end()))
		return [str(self.part.get_num()), fs, common.get_display_size(self.get_size()), flag, start, end]
