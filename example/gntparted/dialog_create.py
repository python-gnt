#!/usr/bin/env python

"""The 'Create Partition' dialog.
"""

import gobject
import gnt
import parted
import common
import sys

class CreateDialog:
	def __init__(self):
		self.reset()

	def reset(self):
		self.window = None
		self.parttype = None
		self.fstype = None
		self.start = None
		self.end = None
		self.device = None
		self.partition = None
		self.mwindow = None

	def set_dev_part(self, dev, part, win):
		self.device = dev
		self.partition = part
		self.mwindow = win

	def show(self):
		if self.window:
			self.window.destroy()
		self.window = win = gnt.Box(homo = False, vert = True)
		
		win.set_toplevel(True)
		win.set_title("Create Partition")
		win.set_pad(0)

		combo = self.parttype = gnt.ComboBox()
		for type, label in (('primary', 'Primary'), ('extended', 'Extended'), ('logical', 'Logical')):
			combo.add_data(type, label)

		combo = self.fstype = gnt.ComboBox()
		for type in common.filesystems:
			combo.add_data(type, type)

		self.start = gnt.Entry('')
		self.end = gnt.Entry('')

		win.add_widget(gnt.Label("Create new partition in " + self.device.device.get_path()))

		win.add_widget(common.create_box(gnt.Label("Partition Type: "), self.parttype, True))
		win.add_widget(common.create_box(gnt.Label("Filesystem: "), self.fstype, True))
		win.add_widget(common.create_box(gnt.Label("Start: "), self.start, True))
		win.add_widget(common.create_box(gnt.Label("End: "), self.end, True))

		def create_partition(button):
			ptype = self.parttype.get_selected_data()
			if ptype == 'extended':	ptype = parted.PARTITION_EXTENDED
			elif ptype == 'logical':	ptype = parted.PARTITION_LOGICAL
			elif ptype == 'primary':	ptype = parted.PARTITION_PRIMARY
			else:	return

			fstype = self.fstype.get_selected_data()
			if fstype not in common.filesystems:
				return
			fstype = parted.file_system_type_get(fstype)
			if fstype is None:
				return

			start = common.convert_size(self.start.get_text())
			end = common.convert_size(self.end.get_text())

			partition = parted.Partition(self.device.disk, ptype, fstype,
					start, end)
			self.device.disk.add_partition(partition)
			sys.stderr.write("created partition: " + str(partition) + 
					" size: " + str(partition.get_geom().get_length()) + "\n")
			partition = self.device.disk.maximize_partition(partition)
			sys.stderr.write("size: " + str(partition))
			sys.stderr.write("write result: " + str(self.device.disk.write()) + "\n")
			self.device.disk.write()
			self.device.device.sync()
			parts = self.device.disk.get_part_list()
			for part in parts:
				fs = part.get_fs_type()
				if fs:
					sys.stderr.write("\tPartition" + str(part.get_num()) + ":" + part.get_fs_type().get_name() + "\n")
			self.mwindow.update_partition_list(None)
			self.window.destroy()
		create = gnt.Button("Create")
		create.connect('activate', create_partition)

		def cancel_dialog(button):
			self.window.destroy()
		cancel = gnt.Button("Cancel")
		cancel.connect('activate', cancel_dialog)

		win.add_widget(common.create_box(create, cancel))
		
		def destroy_cb(win):
			self.reset()
		win.connect('destroy', destroy_cb)
		win.set_alignment(gnt.ALIGN_MID)
		win.show()

createdialog = CreateDialog()
def show_create_dialog(device, partition, mwindow):
	createdialog.set_dev_part(device, partition, mwindow)
	createdialog.show()

