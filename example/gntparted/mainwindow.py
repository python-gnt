#!/usr/bin/env python
"""The main window.
"""

import parted
import gnt
import gpdevice
import gppart
import sys

import dialog_label
import dialog_create
import dialog_delete
import dialog_format

class MainWindow:
	def __init__(self):
		self.devlist = None
		self.partlist = None
		self.window = None

	def show(self):
		self.window = win = gnt.Box(homo = False, vert = True)
		win.set_toplevel(True)
		win.set_title("GntParted")
		win.set_pad(0)

		lists = gnt.Box(homo = False, vert = False)
		lists.set_pad(0)

		self.devlist = devlist = gnt.Tree()
		devlist.set_property('columns', 3)

		devlist.set_column_title(0, 'Path')
		devlist.set_col_width(0, 10)

		devlist.set_column_title(1, 'Type')
		devlist.set_col_width(1, 7)

		devlist.set_column_title(2, 'Size')
		devlist.set_col_width(2, 7)
		devlist.set_column_is_right_aligned(2, True)

		#gnt.set_flag(devlist, 0x08)
		devlist.set_show_title(True)

		devices = parted.get_devices()
		for device in devices:
			gdev = gpdevice.Device(device)
			devlist.add_row_after(gdev, gdev.create_row(), None)

		vbox = gnt.Box(homo = False, vert = True)
		vbox.set_pad(0)
		vbox.add_widget(gnt.Label("List of devices:"))
		vbox.add_widget(devlist)
		lists.add_widget(vbox)

		# List of partitions
		self.partlist = partlist = gnt.Tree()
		partlist.set_property('columns', 6)

		partlist.set_column_title(0, '#')
		partlist.set_col_width(0, 6)

		partlist.set_column_title(1, 'Type')
		partlist.set_col_width(1, 10)

		partlist.set_column_title(2, 'Size')
		partlist.set_col_width(2, 7)
		partlist.set_column_is_right_aligned(2, True)

		partlist.set_column_title(3, 'Flags')
		partlist.set_col_width(3, 5)

		partlist.set_column_title(4, 'Start')
		partlist.set_col_width(4, 7)
		partlist.set_column_is_right_aligned(4, True)

		partlist.set_column_title(5, 'End')
		partlist.set_col_width(5, 7)
		partlist.set_column_is_right_aligned(5, True)

		#gnt.set_flag(partlist, 0x08)
		partlist.set_show_title(True)

		vbox = gnt.Box(homo = False, vert = True)
		vbox.set_pad(0)
		vbox.add_widget(gnt.Label("List of partitions:"))
		vbox.add_widget(partlist)
		lists.add_widget(vbox)

		win.add_widget(lists)

		def update_partition_list_cb(list, old, new):
			self.update_partition_list(None)
		devlist.connect('selection-changed', update_partition_list_cb)

		# The buttons
		hbox = gnt.Box(homo = False, vert = False)

		for operation in [["Label", dialog_label.show_label_dialog],
				["Create", dialog_create.show_create_dialog],
				["Format", dialog_format.show_format_dialog],
				["Resize", None],
				["Move", None],
				["Delete", dialog_delete.show_delete_dialog]]:
			button = gnt.Button(operation[0])
			hbox.add_widget(button)
			def show_operation_dialog(button, callback):
				dev = devlist.get_selection_data()
				part = partlist.get_selection_data()
				callback(dev, part, self)

			if operation[1] is not None:
				button.connect('activate', show_operation_dialog, operation[1])

		win.add_widget(hbox)
		win.set_alignment(gnt.ALIGN_MID)
		win.show()

	def update_device_list(self, gdev):
		if gdev:
			texts = gdev.crate_row()
			col = 0
			for text in texts:
				self.devlist.change_text(gdev, col, text)
				col = col + 1

	def update_partition_list(self, part):
		self.partlist.remove_all()
		sel = self.devlist.get_selection_data()
		ext = None
		if sel is None or sel.disk is None:
			self.partlist.draw()
			return
		for part in sel.disk.get_part_list():
			type = part.get_type()
			if type & parted.PARTITION_METADATA:	continue
			gpart = gppart.Partition(part)
			parent = None
			if type & parted.PARTITION_LOGICAL: parent = ext
			self.partlist.add_row_after(gpart, gpart.create_row(), parent)
			if type & parted.PARTITION_EXTENDED: ext = gpart

