#!/usr/bin/env python

"""Device object.
"""

import gobject
import gnt
import parted
import common

class Device(gobject.GObject):
	def __init__(self, device):
		self.__gobject_init__()
		self.device = device
		self.disk = device.disk_open()

	def __del__(self):
		pass

	def get_size(self):
		return (float(self.device.get_length() * self.device.get_sector_size()))

	def create_row(self):
		dtype = None
		if self.disk:
			dtype = self.disk.get_type()
			if dtype: dtype = dtype.get_name()
		else:
			dtype = "---"
		return [self.device.get_path(), dtype, common.get_display_size(self.get_size())]
