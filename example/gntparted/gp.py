#!/usr/bin/env python

"""
gntparted : Frontend for parted.

Copyright (C) 2007 Sadrul Habib Chowdhury (imadil@gmail.com)
"""

import parted
import sys
import mainwindow
import gnt

def main():
	win = mainwindow.MainWindow()
	parted.init()
	parted.device_probe_all()

	devices = parted.get_devices()
	for device in devices:
		path = device.get_path()
		disk = device.disk_open()
		if not disk:
			print "Device", path, "has no disk!!"
			continue
		print "Device", path, disk.get_type().get_name()
		parts = disk.get_part_list()
		for part in parts:
			fs = part.get_fs_type()
			if fs:
				print "\tPartition", str(part.get_num()), ":", part.get_fs_type().get_name()


	win.show()
	gnt.gnt_main()
	gnt.gnt_quit()

if __name__ == '__main__':
	main()

