import gobject
import gnt

class Row(gobject.GObject):
	def __init__(self):
		self.__gobject_init__()
		self.tagged = False

	def __del__(self):
		pass

	def toggle_tag(self):
		self.tagged = not self.tagged

	def get_row_flag(self):
		if self.tagged:
			return gnt.TEXT_FLAG_BOLD
		return gnt.TEXT_FLAG_NORMAL

gobject.type_register(Row)

