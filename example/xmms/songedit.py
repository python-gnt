#!/usr/bin/env python

"""
Single song edit
"""

import gnt
import os

def safe(id3, attr):
	try:
		return id3[attr]
	except:
		return ""

def pack_box(homo, vert, widgets):
	box = gnt.Box(homo = homo, vert = vert)
	for widget in widgets:
		box.add_widget(widget)
	return box

class SongEdit:
	def __init__(self, xmms):
		self.xmms = xmms
		pass

	def pack_widget(self, label, entry):
		box = gnt.Box(homo = True, vert = False)
		box.add_widget(gnt.Label(label))
		box.add_widget(entry)
		#entry.connect("text_changed", update_template)
		self.win.add_widget(box)

	def get_widget(self):
		self.win = win = gnt.Box(homo = False, vert = True)
		win.set_pad(0)
		win.set_alignment(gnt.ALIGN_MID)

		self.filelabel = gnt.Label("File: ---")
		self.pathlabel = gnt.Label("Path: ---")
		win.add_widget(self.filelabel)
		win.add_widget(self.pathlabel)
		win.add_widget(gnt.Line(False))

		self.title = gnt.Entry("")
		self.pack_widget("Title", self.title)

		self.artist = gnt.Entry("")
		self.pack_widget("Artist", self.artist)

		self.album = gnt.Entry("")
		self.pack_widget("Album", self.album)

		self.year = gnt.Entry("")
		self.pack_widget("Year", self.year)

		self.comment = gnt.Entry("")
		self.pack_widget("Comment", self.comment)

		self.played = gnt.Label("        ")
		self.pack_widget("Times Played", self.played)

		self.fields = [['title', 'Title', self.title],
				  ['artist', 'Artist', self.artist],
				  ['album', 'Album', self.album],
				  ['year', 'Year', self.year],
				  ['comment', 'comment', self.comment],
				  ]

		box = gnt.Box(homo = False, vert = False)
		self.save = gnt.Button("Save")
		box.add_widget(self.save)
		self.reset = gnt.Button("Reset")
		box.add_widget(self.reset)
		self.clear = gnt.Button("Clear All")
		box.add_widget(self.clear)

		def clear_fields(button):
			for field in self.fields:
				field[2].set_text("")
		self.clear.connect('activate', clear_fields)

		def save_fields(button):
			if self.song is None:
				return
			for field in self.fields:
				value = field[2].get_text()
				self.song[field[0]] = value
				self.xmms.medialib_property_set(self.song['id'], field[0], value)
			self.win.destroy()
		self.save.connect('activate', save_fields)

		def reset_fields(button):
			for field in self.fields:
				value = safe(self.song, field[0])
				field[2].set_text(value)
		self.reset.connect('activate', reset_fields)

		win.add_widget(box)

		return self.win

	def set_info(self, song):
		self.song = song
		for field in self.fields:
			field[2].set_text(safe(song, field[0]))
		self.filelabel.set_text("File: " + os.path.basename(safe(song, 'url')))
		self.pathlabel.set_text("Path: " + os.path.dirname(safe(song, 'url')))
		self.played.set_text(str(safe(song, 'timesplayed')))
