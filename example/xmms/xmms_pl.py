#!/usr/bin/env python

"""
xmms-pl : Playlist explorer for XMMS2.

Copyright (C) 2007 Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>

This application is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this application; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301
USA
"""

import xmmsclient
import xmmsclient.glib as xg
import xmmsclient.collections as xc
import gobject
import gnt
import os
import sys
import itertools

import treerow
import songedit
import common
from xmms_list import *

__version__ = "0.0.1alpha"
__author__ = "Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>"
__copyright__ = "Copyright 2007, Sadrul Habib Chowdhury"
__license__ = "GPL"

reload(sys)
sys.setdefaultencoding("utf-8")

class XPList(XList):
	__gproperties__ = {
		'name' : (gobject.TYPE_STRING, 'name of the playlist',
		          'name of the playlist', None, gobject.PARAM_READWRITE)
	}

	__gsignals__ = {
		'playlist-loaded' : (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE,
				())
	}

	__gntbindings__ = {
		'add-entry' : ('add_entry', gnt.KEY_INS),
		'del-entry' : ('del_entry', gnt.KEY_DEL),
		'switch-playlist' : ('switch_playlist', 'w'),
		'create-playlist' : ('create_playlist', 'c'),
		'place-tagged' : ('place_tagged', gnt.KEY_CTRL_P),
		'clear-playlist' : ('clear_playlist', gnt.KEY_CTRL_U),
	}

	def __init__(self, xmms, name = None):
		"""The xmms connection, and the name of the playlist."""
		XList.__init__(self, xmms)
		self.name = name
		self.win = None
		self.init_playlist()
		self.connect('context-menu', self.context_menu)

	def do_get_property(self, prop):
		if prop.name == 'name':
			return self.name
		raise AttributeError, 'unknown property %s' % prop.name

	def do_set_property(self, prop, value):
		if prop.name == 'name':
			self.name = value
		else:
			raise AttributeError, 'unknown property %s' % prop.name

	def context_menu(self, null):
		"""Popup a context menu with the available options."""
		def perform_action(item, data):
			# This is a little weird, so pay attention ...
			# The callback can either bring out a new window, or a new menu.
			# In the first scenario, we want the new window to be given focus.
			#      So we perform the action immediately.
			# In the latter case, we need to first let the active menu hide,
			#      then perform the action, so we call the callback in the next
			#      iteration of the mainloop.
			action, wait = data
			def really_perform_action():
				action(None)
				return False
			if wait:
				gobject.timeout_add(0, really_perform_action)
			else:
				action(None)
		menu = gnt.Menu(gnt.MENU_POPUP)
		for text, action, wait in (('Edit Info', self.edit_entry, False),
		                     ('Remove', self.del_entry, False),
		                     ('Add Files...', self.add_entry, False),
		                     ('Edit Columns ...', self.edit_columns, True),
		                     ('Set Search Column ...', self.search_column, True),
		                     ('Switch Playlist...', self.switch_playlist, True)
		                     ):
			item = gnt.MenuItem(text)
			item.connect('activate', perform_action, [action, wait])
			menu.add_item(item)
		self.position_menu(menu)
		gnt.show_menu(menu)

	def init_playlist(self):
		self.load_playlist()

		# Refresh the playlist if an entry is added/removed etc.
		def refresh_playlist(result):
			info = result.value()
			if info['name'] != self.name:	return   # This playlist didn't change
			if info['type'] == xmmsclient.PLAYLIST_CHANGED_REMOVE:
				# Some entry was removed
				rows = self.get_rows()
				row = rows[info['position']]
				self.remove(row)
			elif info['type'] == xmmsclient.PLAYLIST_CHANGED_ADD or info['type'] == xmmsclient.PLAYLIST_CHANGED_INSERT:
				# Some entry was added
				position = info['position']
				self.add_mlib_song(1 + int(position), info['id'])
			elif info['type'] == xmmsclient.PLAYLIST_CHANGED_MOVE:
				old = info['position']
				new = info['newposition']
				# First, remove the entry
				rows = self.get_rows()
				row = rows[old]
				self.remove(row)
				# Now, find the new entry position
				rows = self.get_rows()
				if new > 0:
					after = rows[new - 1]
				else:
					after = None
				info = row.get_data('song-info')
				[title, artist, album, time] = common.str_info(info)
				# Add it back in the new position
				self.add_row_after(row, [str(new + 1), title, artist, album, time], None, after)
			elif info['type'] == xmmsclient.PLAYLIST_CHANGED_CLEAR:
				self.remove_all()
				self.draw()
			else:
				sys.stderr.write("Unhandled playlist update type " + str(info['type']) + " -- Please file a bug report.\n")
				# XXX: just go ahead and refresh the entire list
				self.load_playlist()
				return
			# Make sure the entry in the 'position' column is correct for the entries
			rows = self.get_rows()
			if rows is not None:
				num = 1
				for row in rows:
					self.change_text(row, self.POSITION, str(num))
					num = num + 1
		self.xmms.broadcast_playlist_changed(refresh_playlist)

	def load_playlist(self):
		"""Get the entries in the list, and populate the tree."""
		def got_list_of_songs_in_pl(result):
			list = result.value()
			pos = itertools.count(1)
			for song in list:
				self.add_mlib_song(int(pos.next()), song)
			self.emit('playlist-loaded')
		if self.win:
			self.win.set_title("XMMS2 Playlist - " + str(self.name))
			self.win.draw()
		self.remove_all()
		self.draw()
		self.xmms.playlist_list_entries(self.name, got_list_of_songs_in_pl)

	def show(self):
		"""Show the playlist inside a window."""
		win = self.win = gnt.Box(homo = False, vert = True)
		win.set_toplevel(True)
		win.set_title("XMMS2 Playlist - " + str(self.name))
		win.add_widget(self)
		width, height = gnt.screen_size()
		self.set_size(width, height)
		win.show()

	def add_entry(self, null):
		"""Add new song(s) in the playlist."""
		fl = gnt.FileSel()
		fl.set_multi_select(True)
		def destroy_fl(b, dlg):
			dlg.destroy()
		fl.cancel_button().connect('activate', destroy_fl, fl)
		def add_files(fl, path, files, dlg):
			for file in dlg.get_selected_multi_files():
				self.xmms.playlist_add_url('file://' + file, self.name)
			dlg.destroy()
		fl.connect('file_selected', add_files, fl)
		fl.show()

	def del_entry(self, null):
		"""Delete selected entry from the playlist."""
		sel = self.get_selection_data()
		if not sel:
			return
		index = self.get_rows().index(sel)
		self.xmms.playlist_remove_entry(index, self.name)

	def switch_playlist(self, null):
		"""Switch the playlist to show in the list."""
		def show_list_menu(res):
			def _load_playlist(item):
				name = item.get_data('playlist-name')
				self.set_property('name', name)
				self.load_playlist()
			list = res.value()
			menu = gnt.Menu(gnt.MENU_POPUP)
			for pl in list:
				name = str(pl)
				item = gnt.MenuItemCheck(name)
				if name == self.name:
					item.set_checked(True)
				item.set_data('playlist-name', name)
				item.connect('activate', _load_playlist)
				menu.add_item(item)
			self.position_menu(menu)
			gnt.show_menu(menu)
		self.xmms.playlist_list(show_list_menu)
		return True

	def create_playlist(self, null):
		"""Create a new playlist."""
		win = gnt.Window()
		win.set_title('Create Playlist')
		win.set_fill(False)
		win.set_alignment(gnt.ALIGN_MID)
		entry = gnt.Entry('')
		button = gnt.Button('Create')

		def create_playlist_cb(w, entry):
			name = entry.get_text()
			def _switch_list(res):
				self.name = name
				self.load_playlist()
			self.xmms.playlist_create(name, _switch_list)
			win.destroy()
		entry.connect('activate', create_playlist_cb, entry)
		button.connect('activate', create_playlist_cb, entry)

		win.add_widget(gnt.Label('Name'))
		win.add_widget(entry)
		win.add_widget(button)
		win.show()
		return True

	def place_tagged(self, null):
		"""Move the tagged entries immediately after the selected entry."""
		entry = self.get_selection_data()
		if not entry:
			return True
		all = self.get_rows()
		index = all.index(entry)
		pos = -1
		for row in all:
			pos = pos + 1
			if not row.tagged:
				continue
			row.toggle_tag()
			if pos > index:
				index = index + 1
			self.xmms.playlist_move(pos, index, self.name)
			if pos < index:
				pos = pos - 1

	def clear_playlist(self, null):
		"""Clear the playlist. Prompt first."""
		win = gnt.Window()
		win.set_title("Confirm")
		win.set_property('vertical', True)
		win.set_fill(False)
		win.set_alignment(gnt.ALIGN_MID)
		clr = gnt.Button("Clear")
		def really_clear_playlist(b, window):
			self.xmms.playlist_clear(self.name)
			window.destroy()
		clr.connect('activate', really_clear_playlist, win)
		cnc = gnt.Button("Cancel")
		def close_window(b, window):
			window.destroy()
		cnc.connect('activate', close_window, win)
		win.add_widget(gnt.Label("Do you really want to remove all entries from this playlist?"))
		win.add_widget(common.pack_widget(False, [clr, cnc]))
		win.show()
		return True

gobject.type_register(XPList)
gnt.register_bindings(XPList)

class XPListCurrent(XPList):
	__gntbindings__ = {
		'play-selected' : ('play_selected', gnt.KEY_ENTER),
		'play' : ('play', 'x'),
		'pause' : ('pause', 'c'),
		'stop' : ('stop', 'v'),
		'play-next' : ('play_next', 'b'),
		'play-prev' : ('play_prev', 'z'),
		'jump-to-current' : ('jump_to_current', gnt.KEY_CTRL_J),
		'selected-next' : ('selected_next', 'q'),
	}

	def __init__(self, xmms):
		XPList.__init__(self, xmms, None)
		def _set_name(res):
			self.name = res.value()
		xmms.playlist_current_active(_set_name)
		self.bold = None
		def _got_current_pos(result):
			self.set_current(result.value())
		xmms.broadcast_playlist_current_pos(cb = _got_current_pos)

	def do_playlist_loaded(self):
		def _got_current_pos(result):
			self.set_current(result.value())
		self.bold = None
		self.xmms.playlist_current_pos(cb = _got_current_pos)

	def do_set_property(self, prop, value):
		XPList.do_set_property(self, prop, value)
		self.xmms.playlist_load(self.name)

	def set_current(self, current):
		if self.bold:
			self.set_row_flags(self.bold, gnt.TEXT_FLAG_NORMAL)
		rows = self.get_rows()
		if not rows: return
		self.bold = rows[int(current)]
		if self.bold:
			self.set_row_flags(self.bold, gnt.TEXT_FLAG_BOLD)

	def play_selected(self, null):
		song = self.get_selection_data()
		if song is None:
			return False
		index = self.get_rows().index(song)
		self.xmms.playback_start()
		def cb(res):
			self.xmms.playback_tickle()
		self.xmms.playlist_set_next(index, cb)
		return True

	def play_next(self, null):
		self.xmms.playlist_set_next_rel(1)
		self.xmms.playback_tickle()
		return True

	def play_prev(self, null):
		self.xmms.playlist_set_next_rel(-1)
		self.xmms.playback_tickle()
		return True

	def play(self, null):
		self.xmms.playback_start()
		return True

	def pause(self, null):
		self.xmms.playback_pause()
		return True

	def stop(self, null):
		self.xmms.playback_stop()
		return True

	def jump_to_current(self, null):
		def got_current_pos(result):
			current = result.value()
			rows = self.get_rows()
			self.set_selected(rows[current])
		self.xmms.playlist_current_pos(cb = got_current_pos)
		return True

	def selected_next(self, null):
		song = self.get_selection_data()
		if song is None:
			return False
		index = self.get_rows().index(song)
		self.xmms.playlist_set_next(index)
		return True

gobject.type_register(XPListCurrent)
gnt.register_bindings(XPListCurrent)

def setup_xmms():
	xmms = xmmsclient.XMMS("pygnt-playlist")
	try:
		xmms.connect(os.getenv("XMMS_PATH"))
		conn = xg.GLibConnector(xmms)
		xqs = XPListCurrent(xmms)
		xqs.show()
	except IOError, detail:
		common.show_error("Connection failed: " + str(detail))

if __name__ == '__main__':
	setup_xmms()
	gnt.gnt_main()
	gnt.gnt_quit()

