#!/usr/bin/env python

"""
xmms-eq : Equalizer for XMMS2.

Copyright (C) 2007 Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>

This application is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this application; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301
USA
"""

import xmmsclient
import xmmsclient.glib as xg
import xmmsclient.collections as xc
import gnt
import common
import os

__version__ = "0.0.1alpha"
__author__ = "Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>"
__copyright__ = "Copyright 2007, Sadrul Habib Chowdhury"
__license__ = "GPL"

class XSlider(gnt.Slider):
	def update_from_xr(self, ret):
		self.set_value(int(float(ret.value())))

def create_label(text):
	label = gnt.Label(text)
	gnt.unset_flag(label, 1 << 8)
	return label

class XQSettings(gnt.Box):
	def __init__(self, xmms):
		gnt.Box.__init__(self, vert = True, homo = False)
		self.xmms = xmms
		self.set_pad(1)
		self.win = None

		def hook_config_val(config, widget, type):
			def update_val(ret):
				if type == 'entry':
					widget.set_text(str(ret.value()))
				elif type == 'slider':
					widget.set_value(int(float(ret.value())))
				elif type == 'check':
					if int(ret.value()) == 0:
						widget.set_checked(False)
					else:
						widget.set_checked(True)
				widget.draw()
			self.xmms.configval_get(config, update_val)

		# Bands
		self.bands = bands = gnt.Slider(False, 10, 30)
		gnt.set_flag(bands, 1 << 8)
		bands.set_step(5)
		val = create_label("  ")
		bands.reflect_label(val)
		self.add_widget(common.pack_widget(False, [create_label("Band"), bands, val]))
		hook_config_val("equalizer.bands", bands, 'slider')
		def band_value_changed(wid, value, refl):
			self.xmms.configval_set("equalizer.bands", str(value))
			pass
		bands.connect('changed', band_value_changed, val)

		self.preamp = preamp = gnt.Slider(False, -20, 20)
		preamp.set_step(1)
		val = create_label(" " * 6)
		preamp.reflect_label(val)
		self.add_widget(common.pack_widget(False, [create_label("Preamp"), preamp, val]))
		hook_config_val("equalizer.preamp", preamp, 'slider')
		def preamp_value_changed(wid, value, refl):
			self.xmms.configval_set("equalizer.preamp", str(value))
		preamp.connect('changed', preamp_value_changed, val)

		self.extra = extra = gnt.CheckBox("Extra Filtering")
		self.add_widget(extra)
		hook_config_val("equalizer.extra_filtering", extra, 'check')
		def extra_toggled(wid):
			self.xmms.configval_set("equalizer.extra_filtering", ["0", "1"][wid.get_checked()])
		extra.connect('toggled', extra_toggled)

		self.legacy = legacy = gnt.CheckBox("Use Legacy")
		self.add_widget(legacy)
		hook_config_val("equalizer.use_legacy", legacy, 'check')
		def legacy_toggled(wid):
			self.xmms.configval_set("equalizer.use_legacy", ["0", "1"][wid.get_checked()])
		legacy.connect('toggled', legacy_toggled)

		self.sliders = []
		self.sliderbox = gnt.Box(vert = False, homo = True)
		self.add_widget(self.sliderbox)

		def initialize_sliders(ret):
			self.update_sliders()
		self.xmms.configval_get("equalizer.use_legacy", initialize_sliders)
		self.xmms.broadcast_configval_changed(self.update_widgets_after_configval_change)

	def show(self):
		win = self.win = gnt.Box(vert = False, homo = False)
		win.set_toplevel(True)
		win.set_title("XMMS2 Equalizer")

		win.add_widget(self)
		win.show()

	def update_widgets_after_configval_change(self, ret):
		value = ret.value()
		key = value.keys()[0]
		value = value[key]
		if key == 'equalizer.use_legacy':
			self.update_sliders()
			self.legacy.set_checked([False, True][int(value) > 0])
		elif key == 'equalizer.bands':
			self.bands.set_value(int(float(value)))
		elif key == 'equalizer.preamp':
			#self.preamp.set_value(int(float(value)))
			pass
		elif key == 'equalizer.extra_filtering':
			self.extra.set_checked([False, True][int(value) > 0])

	def update_sliders(self):
		leg = self.legacy.get_checked()
		self.sliderbox.remove_all()

		name = ""
		count = 0
		spaces = None
		if leg:
			name = "legacy"
			count = 10
			spaces = ["60", "170", "310", "600", "1K", "3K", "6K", "12K", "14K", "16K"]
			width = "1"
		else:
			name = "gain"
			count = 10 #self.bands.get_value()   # -- XXX need a way to specify values for 'spaces' from this
			spaces = ["31", "62", "125", "250", "500", "1K", "2K", "4K", "8K", "16K"]
			width = "2"

		self.sliders = []
		for iter in range(count):
			slider = XSlider(True, -20, 20)
			slider.set_step(1)
			slider.set_large_step(5)
			self.sliders.append(slider)
			nm = ("equalizer.%s%0" + width + "d") % (name, iter)
			self.xmms.configval_get(nm, slider.update_from_xr)
			def update_config_value(wid, val, config):
				self.xmms.configval_set(config, str(val))
			slider.connect('changed', update_config_value, nm)
			box = common.pack_widget(True, [slider, create_label("%-3s" % spaces[iter])], pad = 0)
			self.sliderbox.add_widget(box)
		if self.win is not None:
			self.win.readjust()
			self.win.give_focus_to_child(self.legacy)

def setup_xmms():
	xmms = xmmsclient.XMMS("pygnt-equalizer")
	try:
		xmms.connect(os.getenv("XMMS_PATH"))
		conn = xg.GLibConnector(xmms)
		xqs = XQSettings(xmms)
		xqs.show()
	except IOError, detail:
		common.show_error("Connection failed: " + str(detail))

if __name__ == '__main__':
	setup_xmms()
	gnt.gnt_main()
	gnt.gnt_quit()

