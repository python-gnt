#!/usr/bin/env python

"""
xmms-search : A usable and useful search system for XMMS2.

Copyright (C) 2007 Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>

This application is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this application; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301
USA
"""

import xmmsclient
import xmmsclient.glib as xg
import xmmsclient.collections as xc
import gobject
import gnt
import os
import sys
import itertools

import treerow
import songedit
import common
from xmms_list import *

__version__ = "0.0.1alpha"
__author__ = "Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>"
__copyright__ = "Copyright 2007, Sadrul Habib Chowdhury"
__license__ = "GPL"

reload(sys)
sys.setdefaultencoding("utf-8")

class XSearchResult(XList):
	__gntbindings__ = {
		'add-to-playlist': ('add_to_playlist', gnt.KEY_CTRL_A),
	}
	def add_to_playlist(self, null):
		songs = self.get_rows()
		for song in songs:
			if not song.tagged: continue
			song.toggle_tag()
			self.update_row_flags(song)
			id = song.get_data('song-id')
			self.xmms.playlist_add_id(id)
		return True

gobject.type_register(XSearchResult)
gnt.register_bindings(XSearchResult)

class XSearch(gnt.Box):
	def __init__(self, xmms):
		gnt.Box.__init__(self, False, True)
		self.set_pad(0)
		self.xmms = xmms
		self.entry = gnt.Entry('')
		self.list = XSearchResult(xmms)
		self.list.hide_column(XList.POSITION)
		self.list.hide_column(XList.ALBUM)
		self.add_widget(self.entry)
		self.add_widget(self.list)
		self.entry.connect('activate', self.search)

	def search(self, entry):
		self.list.remove_all()
		string = entry.get_text()
		searchstring = ""
		if ':' in string:
			searchstring = string
		else:
			searchstring = "artist:*%s* OR title:*%s*" % (string, string)
		coll = xmmsclient.xmmsapi.coll_parse(searchstring)
		def got_coll_list(result):
			self.list.remove_all()
			songs = result.value()
			pos = 1
			for song in songs:
				self.list.add_mlib_song(pos, song)
				pos = pos + 1
			self.list.draw()
		ph = treerow.Row()
		string = [""] * self.list.COLUMNS
		string[self.list.TITLE] = "Searching..."
		self.list.add_row_after(ph, string, None)
		self.xmms.coll_query_ids(coll, cb = got_coll_list)

	def standalone(self, title = "XMMS2 Search"):
		self.set_toplevel(True)
		self.set_title(title)

gobject.type_register(XSearch)
gnt.register_bindings(XSearch)

def setup_xmms():
	xmms = xmmsclient.XMMS("pygnt-search")
	try:
		xmms.connect(os.getenv("XMMS_PATH"))
		conn = xg.GLibConnector(xmms)
		xs = XSearch(xmms)
		xs.standalone()
		xs.show()
	except IOError, detail:
		common.show_error("Connection failed: " + str(detail))

if __name__ == '__main__':
	setup_xmms()
	gnt.gnt_main()
	gnt.gnt_quit()

