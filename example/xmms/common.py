import gnt
import sys

def str_info(info):
	title = str(info.get('title', ''))
	artist = str(info.get('artist', ''))
	album = str(info.get('album', ''))
	time = int(info.get('duration', 0))
	time = int(time / 1000)
	time = "%02d:%02d" % (int(time / 60), int(time % 60))
	played = str(info.get('timesplayed', ''))

	return [title, artist, album, time, played]

def debug(message):
	sys.stderr.write(message)

def pack_widget(vert, widgets, pad = 1):
	box = gnt.Box(vert = vert, homo = False)
	box.set_pad(pad)
	for widget in widgets:
		box.add_widget(widget)
	return box

def show_error(message):
	win = gnt.Window()
	win.set_title("Error")
	button = gnt.Button("OK")
	def ok_clicked(but):
		win.destroy()
	button.connect('activate', ok_clicked)
	win.add_widget(pack_widget(True, [gnt.Label(message), button]))
	win.show()

