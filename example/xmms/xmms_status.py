#!/usr/bin/env python

"""
xmms-status : A status widget for XMMS2.

Copyright (C) 2007 Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>

This application is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this application; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301
USA
"""

import xmmsclient
import xmmsclient.glib as xg
import xmmsclient.collections as xc
import gobject
import gnt
import os
import sys

import common

__version__ = "0.0.1alpha"
__author__ = "Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>"
__copyright__ = "Copyright 2007, Sadrul Habib Chowdhury"
__license__ = "GPL"

reload(sys)
sys.setdefaultencoding("utf-8")

class XStatus(gnt.Box):
	"""A generic widget displaying the playback status of XMMS2."""
	PROP_TITLE = 1 << 0
	PROP_ARTIST = 1 << 1
	PROP_ALBUM = 1 << 2
	PROP_DURATION = 1 << 3
	PROP_QUALITY = 1 << 4
	PROP_URL = 1 << 5

	def __init__(self, xmms):
		gnt.Box.__init__(self, False, False)
		self.set_property('vertical', True)
		self.set_pad(0)
		self.xmms = xmms
		self.current = None
		self.playtime = ""
		self.fields = self.PROP_TITLE | self.PROP_ARTIST | self.PROP_DURATION

		self.tv = gnt.TextView()
		self.tv.set_flag(gnt.TEXT_VIEW_NO_SCROLL)
		self.tv.set_flag(gnt.TEXT_VIEW_TOP_ALIGN)

		self.tv.append_text_with_flags("Title: ", gnt.TEXT_FLAG_BOLD)
		self.tv.append_text_with_tag("...", gnt.TEXT_FLAG_NORMAL, "title")
		self.tv.append_text_with_flags("\nArtist: ", gnt.TEXT_FLAG_BOLD)
		self.tv.append_text_with_tag("...", gnt.TEXT_FLAG_NORMAL, "artist")
		self.tv.append_text_with_flags("\nDuration: ", gnt.TEXT_FLAG_BOLD)
		self.tv.append_text_with_tag(".../...", gnt.TEXT_FLAG_NORMAL, "playtime")
		self.tv.set_size(-1, 3)
		gnt.unset_flag(self.tv, gnt.WIDGET_GROW_Y)

		self.status = gnt.Label(" " * max(len("Stopped"), len("Paused"), len("Playing")))
			                              # Hopefully this will make it easier when translation happens
		self.status.set_property("text-flag", gnt.TEXT_FLAG_BOLD);
		gnt.unset_flag(self.status, gnt.WIDGET_GROW_X)

		self.progress = gnt.Slider(False, 1, 0)
		gnt.unset_flag(self.progress, gnt.WIDGET_CAN_TAKE_FOCUS)

		self.add_widget(self.tv)
		self.add_widget(common.pack_widget(False, [self.status, self.progress]))

		# Update playback status
		self.xmms.broadcast_playback_status(self.update_status)
		self.xmms.playback_status(self.update_status)

		# Update playback time
		self.xmms.playback_playtime(self.update_playtime)

		# Update song information
		self.xmms.broadcast_playback_current_id(self.update_song_info)
		self.xmms.playback_current_id(self.update_song_info)

		def medialib_entry_changed(result):
			song = result.value()
			if song == self.current['id']:
				self.update_song_info()
		self.xmms.broadcast_medialib_entry_changed(medialib_entry_changed)

	def update_fields(self):
		self.tv.clear()
		height = 0
		fields = ((self.PROP_TITLE, "Title", "title"),
				(self.PROP_ARTIST, "Artist", "artist"),
				(self.PROP_ALBUM, "Album", "album"),
				(self.PROP_DURATION, "Duration", "playtime"),
				(self.PROP_QUALITY, "Quality", "quality"),
				(self.PROP_URL, "URL", "url"),
			 )

		for field, title, attr in fields:
			if not (self.fields & field): continue
			if height > 0:
				title = "\n" + title
			self.tv.append_text_with_flags(title + ": ", gnt.TEXT_FLAG_BOLD)
			self.tv.append_text_with_tag("...", gnt.TEXT_FLAG_NORMAL, attr)
			height = height + 1

		gnt.set_flag(self.tv, gnt.WIDGET_GROW_Y)
		self.tv.set_size(-1, height)
		gnt.unset_flag(self.tv, gnt.WIDGET_GROW_Y)
		if self.current:
			self.update_song_info()

	def update_status(self, result):
		val = result.value()
		status = "..."
		if val == xmmsclient.PLAYBACK_STATUS_STOP:
			status = "Stopped"
		elif val == xmmsclient.PLAYBACK_STATUS_PLAY:
			status = "Playing"
		elif val == xmmsclient.PLAYBACK_STATUS_PAUSE:
			status = "Paused"
		self.status.set_text(status)

	def update_playtime(self, result):
		val = result.value() / 1000
		self.tv.tag_change("playtime", "%02d:%02d/%s" % (val / 60, val % 60, self.playtime), True)
		self.tv.draw()
		self.progress.set_value(val)
		def restart_signal():
			self.xmms.playback_playtime(self.update_playtime)
			return False
		gobject.timeout_add_seconds(1, restart_signal)

	def update_song(self):
		for field in ('title', 'artist', 'album', 'quality', 'url'):
			self.tv.tag_change(field, str(self.current.get(field, ' ')), True)
		self.tv.draw()

	def update_song_info(self, result = None):
		if result:
			val = result.value()
		else:
			val = self.current['id']
		def received_song_info(result):
			self.current = result.value()
			if self.current is None:
				playtime = 0
				self.current = {'id' : -1, 'title' : '...', 'artist' : '...', 'album' : '...', 'samplerate' : 0, 'bitrate' : 0}
			else:
				playtime = int(self.current.get('duration', 0) / 1000)
			self.current[('client-quality', 'quality')] = "%.1f kHz, %d kb/s" % (self.current.get('samplerate', 0) / 1000, self.current.get('bitrate', 0) / 1000)
			self.progress.set_range(0, playtime)
			self.playtime = "%02d:%02d" % (playtime / 60, playtime % 60)
			self.update_song()
		self.xmms.medialib_get_info(val, received_song_info)

	def show(self):
		win = gnt.Window()
		win.set_pad(0)
		win.set_title("XMMS2 Status")
		win.add_widget(self)
		win.show()

def setup_xmms():
	xmms = xmmsclient.XMMS("pygnt-search")
	try:
		xmms.connect(os.getenv("XMMS_PATH"))
		conn = xg.GLibConnector(xmms)
		xs = XStatus(xmms)
		xs.fields = xs.fields & ~xs.PROP_TITLE
		xs.fields = xs.fields | xs.PROP_URL | xs.PROP_QUALITY
		xs.update_fields()
		xs.show()
	except IOError, detail:
		common.show_error("Connection failed: " + str(detail))

if __name__ == '__main__':
	setup_xmms()
	gnt.gnt_main()
	gnt.gnt_quit()

