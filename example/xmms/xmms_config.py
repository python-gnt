#!/usr/bin/env python

"""
xmms-config : A configuration manager for XMMS2.

Copyright (C) 2007 Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>

This application is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this application; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301
USA
"""

import xmmsclient
import xmmsclient.glib as xg
import xmmsclient.collections as xc
import os
import gnt
import common
import gobject

import treerow

__version__ = "0.0.1alpha"
__author__ = "Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>"
__copyright__ = "Copyright 2007, Sadrul Habib Chowdhury"
__license__ = "GPL"

def compare_configs(row1, row2):
	c1 = row1.name
	c2 = row2.name
	if c1 > c2:
		return 1
	if c1 < c2:
		return -1
	return 0

class ConfigRow(treerow.Row):
	COMPARE_FUNC = compare_configs
	def __init__(self, name, value = None):
		treerow.Row.__init__(self)
		self.name = name
		self.value = value

gobject.type_register(ConfigRow)

class XConfig(gnt.Tree):
	def __init__(self, xmms):
		gnt.Tree.__init__(self)
		self.set_property('columns', 2)
		self.set_col_width(0, 25)
		self.set_col_width(1, 30)
		self.xmms = xmms
		self.rows = {}
		self.enable_sort()

		def got_config_list(result):
			self.remove_all()
			self.rows = {}
			list = result.value()
			for config in list:
				value = list[config]
				self.add_config(config, value)
			self.draw()
		self.xmms.configval_list(got_config_list)

		def update_config_val(result):
			configs = result.value()
			for config in configs:
				value = configs[config]
				if config not in self.rows:
					self.add_config(config, value)
					continue
				row = self.rows[config]
				self.change_text(row, 1, value)
				row.value = value
			self.draw()
		self.xmms.broadcast_configval_changed(update_config_val)

		def start_editing_value(tree):
			row = self.get_selection_data()
			if not row:
				return
			win = gnt.Window()
			win.set_title("Edit Value")
			name = row.name
			value = row.value
			entry = gnt.Entry(value)
			entry.set_size(max(10, len(value) + 1), 1)
			win.add_widget(common.pack_widget(False, [gnt.Label(name), entry]))
			win.show()
			def save_edited_value(entry, window):
				self.xmms.configval_set(name, entry.get_text())
				window.destroy()
			entry.connect('activate', save_edited_value, win)

		self.connect('activate', start_editing_value)

	def add_config(self, config, value):
		def split_config_name(string):
			splits = string.split(".")
			child = splits[-1]
			del splits[-1]
			parent = ".".join(splits)
			return parent, child

		parent, child = split_config_name(config)
		row = ConfigRow(config, value)
		if parent in self.rows:
			prow = self.rows[parent]
		else:
			prow = ConfigRow(config)
			self.rows[parent] = prow
			self.add_row_after(prow, [str(parent), "[]"], None, None)
		self.add_row_after(row, [str(child), str(value)], prow, None)
		self.rows[config] = row

	def show(self):
		win = gnt.Window()
		win.set_title("Configuration")
		win.add_widget(self)
		win.set_maximize(gnt.WINDOW_MAXIMIZE_Y)
		win.show()

def setup_xmms():
	xmms = xmmsclient.XMMS("pygnt-config-editor")
	try:
		xmms.connect(os.getenv("XMMS_PATH"))
		conn = xg.GLibConnector(xmms)
		xqs = XConfig(xmms)
		xqs.show()
	except IOError, detail:
		common.show_error("Connection failed: " + str(detail))

if __name__ == '__main__':
	setup_xmms()
	gnt.gnt_main()
	gnt.gnt_quit()

