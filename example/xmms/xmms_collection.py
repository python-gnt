#!/usr/bin/env python

"""
xmms-collection : A collection editor for XMMS2.

Copyright (C) 2007 Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>

This application is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this application; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301
USA
"""

import xmmsclient
import xmmsclient.glib as xg
import xmmsclient.collections as xc
import gobject
import gnt
import os
import sys
import itertools

from xmms_list import XList
import treerow
import songedit
import common

reload(sys)
sys.setdefaultencoding("utf-8")

def create_row(text):
	row = treerow.Row()
	row.set_data('value', text)
	return row

class XCollection(gnt.Box):
	def __init__(self, xmms):
		gnt.Box.__init__(self, vert = True, homo = False)
		self.set_pad(0)
		self.xmms = xmms
		self.coll = None

		self.list = gnt.ComboBox()
		self.field = gnt.Entry("")
		self.cond = gnt.ComboBox()
		self.value = gnt.Entry("")
		self.invert = gnt.CheckBox("Invert")
		self.andor = gnt.CheckBox("Intersect with the current result")
		self.desc = gnt.TextView()
		self.result = XList(self.xmms)
		self.result.hide_column(self.result.POSITION)

		self.name = gnt.Entry("")
		self.saveas = gnt.ComboBox()

		def got_list_of_collections(result):
			all = result.value()
			self.list.remove_all()
			self.list.add_data("*", "All Media")
			for coll in all:
				self.list.add_data(create_row(coll), coll)
				
		self.xmms.coll_list(cb = got_list_of_collections)

		for field in ("artist", "album", "duration", "samplerate", "bitrate", "timesplayed", "url", "title"):
			self.field.add_suggest(field)
		self.field.set_size(12, 1)

		for cond, dis in (("equals", "is"),
				("matches", "matches"),
				("non-empty", "is not empty"),
				("greater", "is greather than"),
				("less", "is less than"),
				):
			self.cond.add_data(create_row(cond), dis)

		self.desc.set_flag(gnt.TEXT_VIEW_TOP_ALIGN)
		self.desc.set_size(20, 8)

		for sa, dis in (("dynamic", "Dynamic Collection"),
				("playlist", "Playlist"),
				#("queue", "Queue"),
				("pshuffle", "Party Shuffle"),
			):
			self.saveas.add_data(create_row(sa), dis)

		def add_operand(b):
			old = self.coll

			list = self.list.get_selected_data()
			try:
				list = list.get_data('value')
			except:
				pass
			if list == "*":
				l = xc.Universe()
			else:
				l = xc.Reference('Playlists:' + str(list))

			cond = self.cond.get_selected_data().get_data('value')
			if cond == 'equals':
				c = xc.Equals(parent = l, field = self.field.get_text(), value = self.value.get_text())
			elif cond == 'matches':
				c = xc.Match(parent = l, field = self.field.get_text(), value = self.value.get_text())
			elif cond == 'non-empty':
				c = xc.Has(parent = l, field = self.field.get_text())
			elif cond == 'greater':
				c = xc.Greater(parent = l, field = self.field.get_text(), value = self.value.get_text())
			elif cond == 'less':
				c = xc.Smaller(parent = l, field = self.field.get_text(), value = self.value.get_text())
			else:
				c = None

			if c is not None and self.invert.get_checked():
				c = xc.Complement(c)

			if self.coll is None:
				self.coll = c
			else:
				if self.andor.get_checked():
					self.coll = xc.Intersection(self.coll, c)
				else:
					self.coll = xc.Union(self.coll, c)

			def got_list_of_songs(result):
				self.result.remove_all()
				list = result.value()
				if list is None: list = []
				for song in list:
					self.result.add_mlib_song(0, song)
				self.result.draw()
			self.xmms.coll_query_ids(coll = self.coll, cb = got_list_of_songs)

		add = gnt.Button("Add Rule")
		add.connect('activate', add_operand)

		def clear_collecion(b):
			self.result.remove_all()
			self.result.draw()
			self.desc.clear()
			self.desc.draw()
			self.coll = None
		clear = gnt.Button("Clear All")
		clear.connect('activate', clear_collecion)

		box = common.pack_widget(False, [self.list, gnt.Label("songs")])
		box.set_fill(False)
		box.set_alignment(gnt.ALIGN_MID)
		self.add_widget(box)

		box = common.pack_widget(False, [ self.field, self.cond, self.value, self.invert])
		box.set_fill(False)
		box.set_alignment(gnt.ALIGN_MID)
		self.add_widget(box)

		box = common.pack_widget(False, [self.andor, add, clear])
		box.set_fill(False)
		box.set_alignment(gnt.ALIGN_MID)
		self.add_widget(box)

		self.add_widget(common.pack_widget(False, [ self.result]))

		def save_collection(b):
			name = self.name.get_text()
			saveas = self.saveas.get_selected_data().get_data('value')
			if saveas == 'pshuffle':
				coll = xc.PShuffle(self.coll)
				ns = "Collections"
			elif saveas == 'playlist':
				ns = "Playlists"
				def really_save(result):
					coll = xc.IDList()
					coll.ids += result.value()
					self.xmms.coll_save(coll, name, ns)
				self.xmms.coll_query_ids(self.coll, cb = really_save)
				return
			else:
				coll = self.coll
				ns = "Collections"
			self.xmms.coll_save(coll = coll, name = name, ns = ns)
		save = gnt.Button("Save")
		save.connect('activate', save_collection)

		box = common.pack_widget(False, [gnt.Label("Save "), self.name, gnt.Label("as"), self.saveas, save])
		box.set_fill(False)
		box.set_alignment(gnt.ALIGN_MID)
		self.add_widget(box)

	def show(self):
		win = self.win = gnt.Window()
		win.set_title("XMMS2 Collection")
		win.add_widget(self)
		win.set_maximize(gnt.WINDOW_MAXIMIZE_Y)
		win.show()

gobject.type_register(XCollection)
gnt.register_bindings(XCollection)

def setup_xmms():
	xmms = xmmsclient.XMMS("pygnt-collection")
	try:
		xmms.connect(os.getenv("XMMS_PATH"))
		conn = xg.GLibConnector(xmms)
		xqs = XCollection(xmms)
		xqs.show()
	except IOError, detail:
		common.show_error("Connection failed: " + str(detail))

if __name__ == '__main__':
	setup_xmms()
	gnt.gnt_main()
	gnt.gnt_quit()

