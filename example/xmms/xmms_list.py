#!/usr/bin/env python

"""
xmms-list : A generic widget for a list of songs.

Copyright (C) 2007 Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>

This application is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this application; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301
USA
"""

import xmmsclient
import xmmsclient.glib as xg
import xmmsclient.collections as xc
import gobject
import gnt
import os
import sys
import itertools

import treerow
import songedit
import common

reload(sys)
sys.setdefaultencoding("utf-8")

class XList(gnt.Tree):
	"""Generic Song list."""
	POSITION = 0
	TITLE = 1
	ARTIST = 2
	ALBUM = 3
	TIME = 4
	PLAYED = 5
	COLUMNS = 6

	__gntbindings__ = {
		'edit-entry' : ('edit_entry', 'e'),
		'edit-columns' : ('edit_columns', 'C'),
		'search-column' : ('search_column', 's'),
		'toggle-tag' : ('toggle_tag', gnt.KEY_CTRL_T),
	}

	def __init__(self, xmms):
		"""The xmms connection, and the name of the playlist."""
		gnt.Tree.__init__(self)
		self.xmms = xmms
		self.init_tree()
		self.needupdates = []   # A list of medialib id's which we need to poke information for
		self.columns = 0xffff
		self.searchc = self.TITLE
		self.set_search_column(self.searchc)

	def show_column(self, col):
		"""Show the 'col'th column"""
		if self.columns & (1 << col):	return
		self.columns = self.columns | (1 << col)
		self.set_column_visible(col, True)

	def hide_column(self, col):
		"""Hide the 'col'th column"""
		if not (self.columns & (1 << col)): return
		self.columns = self.columns ^ (1 << col)
		self.set_column_visible(col, False)

	def toggle_column(self, col):
		"""Toggle the visibility of the 'col'th column"""
		if self.columns & (1 << col):
			self.hide_column(col)
		else:
			self.show_column(col)

	def got_song_details(self, result):
		"""This is the callback of medialib_get_info. This updates the information about a song in the playlist."""
		info = result.value()
		id = info.get('id', None)
		if not id:
			return

		[title, artist, album, time, played] = common.str_info(info)

		for row in self.get_rows():
			mlib = row.get_data('song-id')
			if mlib != id:	continue
			row.set_data('song-info', info)
			self.change_text(row, self.ARTIST, artist)
			self.change_text(row, self.ALBUM, album)
			self.change_text(row, self.TITLE, title)
			self.change_text(row, self.TIME, time)
			self.change_text(row, self.PLAYED, played)

	def update_info(self):
		# Update information about some songs, if necessary
		for song in self.needupdates:
			# song is the medialib id
			self.xmms.medialib_get_info(song, self.got_song_details)
		self.needupdates = []
		return False   # That's it! We're done!!

	def queue_update(self, song):
		"""Queue request to update information about a song."""
		# Instead of immediately updating information about the song,
		# we wait a little. This is to avoid having to request info
		# about the song multiple times when more than one property
		# of the song is changed.
		if len(self.needupdates) == 0:
			gobject.timeout_add(500, self.update_info)
		self.needupdates.append(song)

	def init_tree(self):
		self.set_property('columns', self.COLUMNS)

		self.set_show_title(True)

		# Position
		self.set_column_title(self.POSITION, "#")
		self.set_column_is_right_aligned(self.POSITION, True)
		self.set_col_width(self.POSITION, 5)
		self.set_column_resizable(self.POSITION, False)

		# Title
		self.set_column_title(self.TITLE, "Title")
		self.set_col_width(self.POSITION, 20)

		# Artist
		self.set_column_title(self.ARTIST, "Artist")
		self.set_col_width(self.ARTIST, 20)

		# Album
		self.set_column_title(self.ALBUM, "Album")
		self.set_col_width(self.ALBUM, 20)

		# Time
		self.set_column_title(self.TIME, "Time")
		self.set_col_width(self.TIME, 5)
		self.set_column_resizable(self.TIME, False)

		# Times played
		self.set_column_title(self.PLAYED, "<3")
		self.set_col_width(self.PLAYED, 4)
		self.set_column_resizable(self.PLAYED, False)
		self.set_column_is_right_aligned(self.PLAYED, True)

		# Make sure that if some metadata of a song changes, we update the playlist accordingly
		def medialib_entry_changed(result):
			# song is the medialib id of the song
			song = result.value()
			if song not in self.needupdates:
				self.queue_update(song)
		self.xmms.broadcast_medialib_entry_changed(medialib_entry_changed)

	def edit_entry(self, null):
		"""Edit medialib information about the selected entry."""
		sel = self.get_selection_data()
		if not sel:
			return
		info = sel.get_data('song-info')
		if not info:
			common.show_error("Do not have any editable information about this song.")
			return
		edit = songedit.SongEdit(self.xmms)
		win = edit.get_widget()
		edit.set_info(info)
		win.set_toplevel(True)
		win.set_title("Edit Song Information")
		win.show()

	def add_entry(self, null):
		"""Add new song(s) in the playlist."""
		fl = gnt.FileSel()
		fl.set_multi_select(True)
		def destroy_fl(b, dlg):
			dlg.destroy()
		fl.cancel_button().connect('activate', destroy_fl, fl)
		def add_files(fl, path, files, dlg):
			for file in dlg.get_selected_multi_files():
				self.xmms.playlist_add_url('file://' + file, self.name)
			dlg.destroy()
		fl.connect('file_selected', add_files, fl)
		fl.show()

	def del_entry(self, null):
		"""Delete selected entry from the playlist."""
		sel = self.get_selection_data()
		if not sel:
			return
		index = self.get_rows().index(sel)
		self.xmms.playlist_remove_entry(index, self.name)

	def position_menu(self, menu):
		"""Position a menu so that it appears near the selected row in the tree."""
		x, y = self.get_position()
		width, height = self.get_size()
		menu.set_position(x + width, y + self.get_selection_visible_line() + 3)

	def desc_columns(self):
		"""Describe the columns in the tree."""
		return (("Position", self.POSITION), ("Title", self.TITLE), ("Artist", self.ARTIST),
			("Album", self.ALBUM), ("Time", self.TIME), ("# Played", self.PLAYED))

	def edit_columns(self, null):
		"""Change visibility of the columns."""
		menu = gnt.Menu(gnt.MENU_POPUP)
		def toggle_flag(item):
			flag = item.get_data('column')
			self.toggle_column(flag)
			self.draw()
		for text, col in self.desc_columns():
			item = gnt.MenuItemCheck("Show " + text)
			item.set_data('column', col)
			if self.columns & (1 << col):
				item.set_checked(True)
			item.connect('activate', toggle_flag)
			menu.add_item(item)
		self.position_menu(menu)
		gnt.show_menu(menu)

	def search_column(self, null):
		"""Set the column to use for type-ahead search."""
		def change_search(item):
			col = item.get_data('column')
			self.set_search_column(col)
			self.searchc = col
		menu = gnt.Menu(gnt.MENU_POPUP)
		for text, col in self.desc_columns():
			item = gnt.MenuItemCheck("Search " + text)
			item.set_data('column', col)
			item.connect('activate', change_search)
			if col == self.searchc:
				item.set_checked(True)
			menu.add_item(item)
		self.position_menu(menu)
		gnt.show_menu(menu)

	def got_song_details(self, result):
		"""This is the callback of medialib_get_info. This updates the information about a song in the playlist."""
		info = result.value()
		id = info.get('id', None)
		if not id:
			return

		[title, artist, album, time, played] = common.str_info(info)

		rows = self.get_rows()
		if not rows:
			return

		for row in rows:
			mlib = row.get_data('song-id')
			if mlib != id:	continue
			row.set_data('song-info', info)
			self.change_text(row, self.ARTIST, artist)
			self.change_text(row, self.ALBUM, album)
			self.change_text(row, self.TITLE, title)
			self.change_text(row, self.TIME, time)
			self.change_text(row, self.PLAYED, played)

	def add_mlib_song(self, position, mlibid):
		position = int(position) - 1
		def add_row_first(result):
			row = treerow.Row()
			row.set_data('song-id', mlibid)
			rows = self.get_rows()
			after = None
			if not rows:
				rows = []
			if rows is not None and len(rows) >= position > 0:
				after = rows[position - 1]
			# Add the entry with empty information first. Then request the data
			self.add_row_after(row, [str(position + 1)] + ([""] * (self.COLUMNS - 1)), None, after)
			self.got_song_details(result)
		self.xmms.medialib_get_info(mlibid, add_row_first)

	def update_row_flags(self, row):
		self.set_row_flags(row, row.get_row_flag())

	def toggle_tag(self, null):
		"""Tag/untag the selected entry in the playlist."""
		entry = self.get_selection_data()
		if not entry:
			return True
		entry.toggle_tag()
		self.set_row_flags(entry, entry.get_row_flag())
		self.perform_action_named('move-down')
		return True

gobject.type_register(XList)
gnt.register_bindings(XList)

