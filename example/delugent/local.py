from deluge.ui.client import aclient as client
from deluge.configmanager import ConfigManager

configs = {}
gntui = {}

def configs_init():
	def _on_get_config(config):
		global configs
		configs = config
	client.get_config(_on_get_config)

	global gntui
	DEFAULT_PREFS = {
		"load_torrent_location" : "/tmp",
		"torlist.columns.name" : "1",
		"torlist.columns.state" : "1",
		"torlist.columns.done" : "1",
		"torlist.columns.size" : "1",
		"torlist.columns.seed" : "1",
		"torlist.columns.peer" : "1",
		"torlist.columns.dlspeed" : "1",
		"torlist.columns.upspeed" : "1",
		"torlist.columns.eta" : "1",
	}
	gntui = ConfigManager("gntui.conf", DEFAULT_PREFS)

def configs_uninit():
	if gntui:
		gntui.save()

