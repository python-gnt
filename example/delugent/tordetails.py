from deluge.ui.client import aclient as client
import os.path
import deluge.common
from deluge.log import LOG as log

import gnt
import gobject
import common
import local

from torfilelist import TorFileList

class TorFileRow(gobject.GObject):
	def decide_priority(self):
		if not self.enabled:
			return "Skip"
		if self.priorities == 1:
			return "Normal"
		elif self.priorities < 7:
			return "Higher"
		else:
			return "Highest"

	def get_row_flag(self):
		if not self.enabled:
			return gnt.TEXT_FLAG_DIM
		elif self.progress == 1.0:
			return gnt.TEXT_FLAG_BOLD
		else:
			return gnt.TEXT_FLAG_NORMAL

	def __init__(self, file):
		self.__gobject_init__()
		self.path = file['path']
		self.size = file['size']
		self.pieces = 0 #len(file['priorities'])
		self.priorities = 1 #file['priorities']
		self.enabled = file['priority'] #len([p for p in self.priorities if p > 0]) > 0
		self.progress = file['progress']

	def info(self):
		prg = "%.1f" % (self.progress * 100)
		return [self.path, "%s (%5s%%)" % (common.show_size(self.size), prg), self.decide_priority()]
gobject.type_register(TorFileRow)

_tor_details = {}

class TorDetails(gnt.Window):
	KEYS = [
		'name',
		'save_path',
		'num_files',
		'state',
		'tracker',
		'total_done',
		'total_size',
		'download_payload_rate',
		'total_uploaded',
		'total_payload_upload',
		'upload_payload_rate',
		'num_pieces',
		'piece_length',
		'num_seeds', 'total_seeds',
		'num_peers', 'total_peers',
		'eta',
		'ratio',
		'distributed_copies',
		'files',
		'file_priorities',
		'file_progress',
		'is_seed',
	]
	DEFAULT = gnt.gnt_color_add_pair(-1, -1)
	BLACK = gnt.gnt_color_add_pair(0, -1)
	RED = gnt.gnt_color_add_pair(1, -1)
	GREEN = gnt.gnt_color_add_pair(2, -1)
	YELLOW = gnt.gnt_color_add_pair(3, -1)
	BLUE = gnt.gnt_color_add_pair(4, -1)
	MAGENTA = gnt.gnt_color_add_pair(5, -1)
	CYAN = gnt.gnt_color_add_pair(6, -1)
	WHITE = gnt.gnt_color_add_pair(7, -1)

	INTERVAL = 5   # Update the information every 5 seconds

	def new(cls, torrent_id):
		if torrent_id in _tor_details:
			return _tor_details[torrent_id]
		r = cls(torrent_id)
		_tor_details[torrent_id] = r
		return r
	new = classmethod(new)

	def __init__(self, torrent_id):
		gnt.Window.__init__(self)
		self.torrent = torrent_id
		self.set_property('vertical', True)
		self.set_property('homogeneous', False)
		self.set_alignment(gnt.ALIGN_MID)
		self.set_pad(0)
		self.set_fill(True)
		self.set_title("Details")
		self.read_status()

		def add_info(info):
			string = ""
			tv = gnt.TextView()
			for label, value in info:
				label = "%15s: " % label
				tv.append_text_with_flags(label, gnt.TEXT_FLAG_BOLD)
				tv.append_text_with_flags(value + "\n", gnt.TEXT_FLAG_NORMAL)
				string = string + label + value + "\n"
			tv.set_flag(gnt.TEXT_VIEW_TOP_ALIGN | gnt.TEXT_VIEW_NO_SCROLL)
			w, h = gnt.get_text_bound(string.strip())
			tv.set_size(w + 3, h)
			gnt.unset_flag(tv, gnt.WIDGET_GROW_Y)
			return tv

		status = self.status
		top = (
			("Name", "%s" % status['name']),
			("Path", "%s" % status['save_path']),
			("# of files", "%s" % status['num_files']),
			("Status", status['state']),
			("Tracker", status['tracker']),
		)
		self.add_widget(add_info(top))
		self.add_widget(gnt.Line(False))

		"""
		left = (
			("Downloaded", "%s (%s)" % (deluge.common.fsize(float(status['total_done'])),
					deluge.common.fsize(float(status['total_size'])))),
			("Download Speed", "%s" % deluge.common.fspeed(float(status['download_payload_rate']))),
			("Uploaded", "%s (%s)" % (deluge.common.fsize(float(status['total_uploaded'])),
					deluge.common.fsize(float(status['total_payload_upload'])))),
			("Upload Speed", "%s" % deluge.common.fspeed(float(status['upload_payload_rate']))),
			("Pieces", "%s (%s)" % (status['num_pieces'], deluge.common.fsize(status['piece_length']))),
		)

		right = (
			("Seeders", "%s (%s)" % (status['num_seeds'], status['total_seeds'])),
			("Peers", "%s (%s)" % (status['num_peers'], status['total_peers'])),
			("ETA", "%s" % deluge.common.ftime(status['eta'])),
			("Share Ratio", "%.1f" % status['ratio']),
			("Availability", "%.1f" % status['distributed_copies']),
		)

		files = gnt.Tree()
		files.set_property('columns', 3)
		files.set_col_width(1, 13)
		files.set_column_resizable(1, False)
		files.set_col_width(2, 10)
		files.set_show_title(True)
		files.set_column_title(0, 'File')
		files.set_column_title(1, 'Size')
		files.set_column_title(2, 'Priority')
		files.set_column_is_right_aligned(1, True)
		priorities = status['file_priorities']
		progress = status['file_progress']
		for i, file in enumerate(status['files']):
			if i < len(priorities):
				file['priority'] = priorities[i]
			else:
				file['priority'] = 1
			file['progress'] = progress[i]
			torfile = TorFileRow(file)
			files.add_row_after(torfile, torfile.info(), None)
			files.set_row_flags(torfile, torfile.get_row_flag())
		self.add_widget(files)
		"""

		tv = gnt.TextView()
		tv.set_flag(gnt.TEXT_VIEW_TOP_ALIGN | gnt.TEXT_VIEW_NO_SCROLL)
		tv.text = ""
		def append_row(color, llabel, ltext, rlabel, rtext):
			if len(tv.text) > 0:
				tv.append_text_with_flags("\n", 0)
				tv.text = tv.text + "\n"
			tv.append_text_with_flags("%15s: " % llabel, gnt.TEXT_FLAG_NORMAL | gnt.gnt_color_pair(color))
			tv.append_text_with_tag("%-25s" % ltext, gnt.TEXT_FLAG_BOLD | gnt.gnt_color_pair(color), ltext)
			tv.append_text_with_flags("%15s: " % rlabel, gnt.TEXT_FLAG_NORMAL | gnt.gnt_color_pair(color))
			tv.append_text_with_tag("%-15s" % rtext, gnt.TEXT_FLAG_BOLD | gnt.gnt_color_pair(color), rtext)
			tv.text = tv.text + "%15s: %-25s %15s: %-15s" % (llabel, ltext, rlabel, rtext)

		if not status['is_seed']:
			append_row(self.GREEN, "Downloaded", "download", "Speed", "dlspeed")

		append_row(self.BLUE, "Uploaded", "upload", "Speed", "upspeed")
		append_row(self.CYAN, "Seeds", "seeds", "Peers", "peers")
		append_row(self.MAGENTA, "Pieces", "pieces", "ETA", "eta")
		append_row(self.DEFAULT, "Share Ratio", "share", "Availability", "availability")

		w, h = gnt.get_text_bound(tv.text)
		tv.set_size(w, h)
		gnt.unset_flag(tv, gnt.WIDGET_GROW_Y)

		self.add_widget(tv)

		self.files = TorFileList(torrent_id)
		self.add_widget(self.files)

		self.statistics = tv
		self.refresh(False)
		self.update_timer = gobject.timeout_add_seconds(self.INTERVAL, self.refresh)

		ok = gnt.Button("OK")
		def close_info_dlg(b, self):
			self.destroy()
			del self
		ok.connect('activate', close_info_dlg, self)
		box = gnt.Box(False, False)
		box.add_widget(ok)

		self.add_widget(box)

		def remove_self(self):
			del _tor_details[torrent_id]
			gobject.source_remove(self.update_timer)
		self.connect('destroy', remove_self)

	def refresh(self, reread=True):
		if reread:
			self.read_status()
		status = self.status
		for tag, text in (
					("download", "%-25s" % ("%s (%s)" % \
						(deluge.common.fsize(float(status['total_done'])),
							deluge.common.fsize(float(status['total_size']))))),
					("dlspeed", "%-15s" % deluge.common.fspeed(float(status['download_payload_rate']))),
					("upload", "%-25s" % ("%s (%s)" % \
						(deluge.common.fsize(float(status['total_uploaded'])),
							deluge.common.fsize(float(status['total_payload_upload']))))),
					("upspeed", "%-15s" % deluge.common.fspeed(float(status['upload_payload_rate']))),
					("seeds", "%-25s" % ("%s (%s)" % (status['num_seeds'], status['total_seeds']))),
					("peers", "%-15s" % ("%s (%s)" % (status['num_peers'], status['total_peers']))),
					("pieces", "%-25s" % ("%s (%s)" % (status['num_pieces'], deluge.common.fsize(status['piece_length'])))),
					("eta", "%-15s" % deluge.common.ftime(status['eta'])),
					("share", "%-25s" % ("%.1f" % status['ratio'])),
					("availability", "%.1f" % status['distributed_copies']),
				):
			self.statistics.tag_change(tag, text, True)
		self.statistics.draw()
		self.files.update(self.status)
		return True

	def read_status(self):
		def _on_torrent_status(status):
			self.status = status

		client.get_torrent_status(_on_torrent_status, self.torrent, TorDetails.KEYS)
		client.force_call()


gobject.type_register(TorDetails)

