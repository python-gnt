
"""Dialog showing the list and status of files in an active torrent"""

from deluge.ui.client import aclient as client
import os.path
import deluge.common
from deluge.log import LOG as log

import gnt
import gobject
import common
import local

_tor_files = {}

class TorFile(gobject.GObject):
	def __init__(self, file):
		self.__gobject_init__()
		self.update(file)

	def update(self, file):
		self.path = file['path']
		self.size = file['size']
		self.pieces = 0 #len(file['priorities'])
		self.priorities = 1 #file['priorities']
		self.enabled = file['priority'] #len([p for p in self.priorities if p > 0]) > 0
		self.progress = file['progress']

	def decide_priority(self):
		if not self.enabled:
			return "Skip"
		if self.priorities == 1:
			return "Normal"
		elif self.priorities < 7:
			return "Higher"
		else:
			return "Highest"

	def get_row_flag(self):
		if not self.enabled:
			return gnt.TEXT_FLAG_DIM
		elif self.progress == 1.0:
			return gnt.TEXT_FLAG_BOLD
		else:
			return gnt.TEXT_FLAG_NORMAL

	def display(self):
		prg = "%.1f" % (self.progress * 100)
		return [os.path.basename(self.path), "%s (%5s%%)" % (common.show_size(self.size), prg), self.decide_priority()]

	def __del__(self):
		pass

gobject.type_register(TorFile)

class TorFileList(gnt.Tree):
	def __init__(self, torrent):
		gnt.Tree.__init__(self)
		self.set_property('columns', 3)
		self.set_property('expander-level', 1)

		self.set_col_width(0, 40)
		self.set_col_width(1, 15)
		self.set_column_resizable(1, False)
		self.set_column_is_right_aligned(1, True)
		self.set_col_width(2, 10)
		self.set_show_title(True)
		self.set_column_title(0, 'File')
		self.set_column_title(1, 'Size')
		self.set_column_title(2, 'Priority')

		self.files = None
		self.torrent = torrent

	def update(self, status):
		if not self.files:
			self.files = {}
			for i, file in enumerate(status['files']):
				file['progress'] = status['file_progress'][i]
				file['priority'] = status['file_priorities'][i]
				row = TorFile(file)
				self.add_row_after(row, row.display(), None)
				self.set_row_flags(row, row.get_row_flag())
				self.files[file['index']] = row
		else:
			for i, file in enumerate(status['files']):
				file['progress'] = status['file_progress'][i]
				file['priority'] = status['file_priorities'][i]
				row = self.files[file['index']]
				row.update(file)
				for i, text in enumerate(row.display()):
					self.change_text(row, i, text)

gobject.type_register(TorFileList)

