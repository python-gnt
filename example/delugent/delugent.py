#!/usr/bin/env python

"""
delugent : Frontend for deluge.

This is meant to work with deluge 0.60 (and above, I am sure), which has
a core-ui split. This is a work in progress.

Copyright (C) 2007, 2008 Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>

This application is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This application is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this application; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02111-1301
USA
"""

import profile
import deluge.common
from deluge.log import LOG as log
from deluge.ui.client import aclient as client

import sys
import os.path

import gobject

gobject.threads_init()

import gnt
import signals
import local
import common
from torfileselector import TorFileSelector
from torsettings import TorSettings
from tordetails import TorDetails

status_keys = {
			"name" : "Name",
			"save_path" : "Path",
			"state" : "Status",
			"tracker_status" : "Summary",
			"total_size" : "Size",
			"total_done" : "Downloaded",
			"total_payload_upload" : "Uploaded",
			"progress" : "Progress",
			"num_seeds" : "# of seeds",
			"total_seeds" : "Total seeds",
			"num_peers" : "# of peers",
			"total_peers" : "Total peers",
			"eta" : "ETA",
			"download_payload_rate" : "Download speed",
			"upload_payload_rate" : "Upload speed",
			"ratio" : "Share Ratio",
			"distributed_copies" : "Availability",
			"total_uploaded" : "Uploaded",
			"num_files" : "# of files",
			"num_pieces" : "# of pieces",
			"piece_length" : "Piece length",
			"files": "Files",
			"file_priorities" : "File Priorities",
			"file_progress" : "File Progress",
		}

def compare_tor_row(row1, row2):
	v1 = row1.row[TorRow.compare_column]
	v2 = row2.row[TorRow.compare_column]
	if not TorRow.compare_asc:
		v1, v2 = v2, v1
	if v1 < v2:
		return -1
	elif v1 > v2:
		return 1
	return 0

def bold_label_and_flag(text, flag):
	label = gnt.Label(text)
	label.set_property("text-flag", flag)
	return label

class TorRow(gobject.GObject):
	COL_NAME = 0
	COL_STATE = 1
	COL_SIZE = 3
	COL_PROG = 2
	COL_SD   = 4
	COL_PR   = 5
	COL_DS   = 6
	COL_US   = 7
	COL_ETA  = 8
	COLS     = 9

	COMPARE_FUNC = compare_tor_row
	compare_column = COL_NAME
	compare_asc = True

	def __init__(self, id):
		self.__gobject_init__()
		self.id = id
		self.row = [""] * self.COLS

	def __del__(self):
		pass

	def info(self, callback):
		keys = ['state',
			'name',
			'total_size',
			'progress',
			'num_peers',
			'num_seeds',
			'download_payload_rate',
			'upload_payload_rate',
			'eta']
		def _got_torrent_status(status):
			state = status.get("state", 0)
			name = str(status.get("name", ""))
			size = int(status.get("total_size", 0))
			progress = float(status.get("progress", 0.0))
			peers = str(status.get("num_peers", 0))
			seeds = str(status.get("num_seeds", 0))
			dlspeed = float(status.get("download_payload_rate", 0.0))
			upspeed= float(status.get("upload_payload_rate", 0.0))
			eta = float(status.get("eta", 0.0))

			self.row[self.COL_NAME] = name
			self.row[self.COL_STATE] = state
			self.row[self.COL_SIZE] = common.show_size(size)
			self.row[self.COL_PROG] = "%.1lf" % progress
			self.row[self.COL_SD] = str(seeds)
			self.row[self.COL_PR] = str(peers)
			self.row[self.COL_DS] = common.show_speed(dlspeed)
			self.row[self.COL_US] = common.show_speed(upspeed)
			self.row[self.COL_ETA] = str(eta)
			callback(self.row)
		client.get_torrent_status(_got_torrent_status, self.id, keys)

gobject.type_register(TorRow)

class TorList(gnt.Tree):
	__gntbindings__ = {
		'info' : ('show_info', 'i'),
		'toggle_border' : ('toggle_border', 't'),
		'change-sort' : ('change_sort_column', 'S'),
	}

	norm_color = gnt.gnt_color_add_pair(-1, -1)
	row_colors = {
		"Downloading" : gnt.gnt_color_add_pair(gnt.gnt_colors_get_color("green"), -1),
		"Paused" : gnt.gnt_color_add_pair(gnt.gnt_colors_get_color("gray"), -1),
		# "Seeding" : gnt.gnt_color_add_pair(-1, gnt.gnt_colors_get_color("green")), # seeding
		"Seeding" : gnt.gnt_color_add_pair(gnt.gnt_colors_get_color("cyan"), -1),
		"Error" : gnt.gnt_color_add_pair(gnt.gnt_colors_get_color("red"), gnt.gnt_colors_get_color("black")),
	}

	def __init__(self):
		gnt.Tree.__init__(self)
		self.set_property('columns', TorRow.COLS)
		self.set_show_title(True)
		self.separator = False
		self.set_show_separator(self.separator)

		self.titles = {TorRow.COL_NAME : "Name",
			TorRow.COL_STATE : "State",
			TorRow.COL_SIZE  : "Size",
			TorRow.COL_PROG  : "   %",
			TorRow.COL_SD    : "Sd",
			TorRow.COL_PR    : "Pr",
			TorRow.COL_DS    : "DL Sp",
			TorRow.COL_US    : "Up Sp",
			TorRow.COL_ETA   : "ETA"
		}
		widths = {TorRow.COL_NAME : 30,
			TorRow.COL_STATE : 7,
			TorRow.COL_SIZE  : 5,
			TorRow.COL_PROG  : 6,
			TorRow.COL_SD    : 3,
			TorRow.COL_PR    : 3,
			TorRow.COL_DS    : 6,
			TorRow.COL_US    : 6,
			TorRow.COL_ETA   : 6
		}

		# Set column titles
		for col in self.titles:
			self.set_column_title(col, self.titles[col])
		# Set column widths and alignments
		for col in widths:
			self.set_col_width(col, widths[col])
		for col in [TorRow.COL_ETA, TorRow.COL_SD, TorRow.COL_PR, TorRow.COL_DS, TorRow.COL_US, TorRow.COL_SIZE, TorRow.COL_PROG]:
			self.set_column_resizable(col, False)
		for col in [TorRow.COL_SIZE, TorRow.COL_DS, TorRow.COL_SD, TorRow.COL_PR, TorRow.COL_US, TorRow.COL_PROG]:
			self.set_column_is_right_aligned(col, True)
		for col, name in (
				(TorRow.COL_NAME,  "name"),
				(TorRow.COL_STATE, "state"),
				(TorRow.COL_SIZE,  "size"),
				(TorRow.COL_PROG,  "done"),
				(TorRow.COL_SD,    "seed"),
				(TorRow.COL_PR,    "peer"),
				(TorRow.COL_DS,    "dlspeed"),
				(TorRow.COL_US,    "upspeed"),
				(TorRow.COL_ETA,   "eta"),
			):
			self.set_column_visible(col, int(local.gntui["torlist.columns." + name]))

		self.connect('context-menu', self.context_menu)

	def context_menu_change_column(self):
		menu = gnt.Menu(gnt.MENU_POPUP)

		# Add a 'change columns' menu
		def _change_column(item):
			name = item.get_data('column-name')
			col = item.get_data('column-no')
			checked = item.get_checked()
			local.gntui[name] = str(int(checked))
			self.set_column_visible(col, checked)
		for col, name in (
				(TorRow.COL_NAME,  "name"),
				(TorRow.COL_STATE, "state"),
				(TorRow.COL_SIZE,  "size"),
				(TorRow.COL_PROG,  "done"),
				(TorRow.COL_SD,    "seed"),
				(TorRow.COL_PR,    "peer"),
				(TorRow.COL_DS,    "dlspeed"),
				(TorRow.COL_US,    "upspeed"),
				(TorRow.COL_ETA,   "eta"),
			):
			item = gnt.MenuItemCheck(self.titles[col])
			name = 'torlist.columns.' + name
			item.set_data('column-name', name)
			item.set_data('column-no', col)
			item.set_checked(local.gntui[name] == '1')
			menu.add_item(item)
			item.connect('activate', _change_column)
		return menu

	def context_menu(self, null):
		menu = gnt.Menu(gnt.MENU_POPUP)

		item = gnt.MenuItem("Change Columns")
		item.set_submenu(self.context_menu_change_column())
		menu.add_item(item)

		item = gnt.MenuItem("Details")
		def _on_details(item):
			self.perform_action_named('info')
		item.connect('activate', _on_details)
		menu.add_item(item)

		# Change upload/download speed etc.
		item = gnt.MenuItem("Change Settings...")
		def _on_change_settings(item):
			torrent = self.get_selection_data()
			change = TorSettings.new(torrent.id)
			change.show()
			change.present()
		item.connect('activate', _on_change_settings)
		menu.add_item(item)

		item = gnt.MenuItem("Delete")
		menu.add_item(item)

		self.position_menu(menu)
		gnt.show_menu(menu)

	def position_menu(self, menu):
		"""Position a menu so that it appears near the selected row in the tree."""
		x, y = self.get_position()
		width, height = self.get_size()
		menu.set_position(x + width, y + self.get_selection_visible_line() + 3)
	def get_row(self, id):
		tors = self.get_rows()
		for tor in tors:
			if tor.id == id:
				return tor
		return None

	def remove_torrent(self, id):
		tor = self.get_row(id)
		if tor:	self.remove(tor)

	def add_torrent(self, id):
		row = TorRow(id)
		def _add_row(rinfo):
			self.add_row_after(row, rinfo, None, None)
			self.update_torrent_info(row, rinfo)
		row.info(_add_row)

	def update_torrent_info(self, id, rinfo):
		for i in range(0, TorRow.COLS):
			self.change_text(id, i, rinfo[i])
		if rinfo[TorRow.COL_STATE] in TorList.row_colors:
			self.set_row_color(id, TorList.row_colors[rinfo[TorRow.COL_STATE]])
		else:
			self.set_row_color(id, TorList.norm_color)

	def update_torrent(self, id, tor = None):
		if not tor:
			tor = self.get_row(id)
		def _change_text(rinfo):
			self.update_torrent_info(tor, rinfo)
		tor.info(_change_text)

	def update_all(self):
		gnt.unset_flag(self, gnt.WIDGET_MAPPED)
		rows = self.get_rows()
		if not rows: return
		for tor in rows:
			self.update_torrent(tor.id, tor)
		gnt.set_flag(self, gnt.WIDGET_MAPPED)
		self.draw()

	def show_info(self, null):
		tor = self.get_selection_data()
		if not tor: return True
		show_details(tor.id)
		return True

	def toggle_border(self, null):
		self.separator = not self.separator
		self.set_show_separator(self.separator)
		self.draw()
		return True

	def change_sort_column(self, null):
		def sort_col_cb(item, col):
			if TorRow.compare_column == col:
				TorRow.compare_asc = not TorRow.compare_asc
			else:
				TorRow.compare_column = col
			rows = self.get_rows()
			for row in rows:
				self.sort_row(row)
		menu = gnt.Menu(gnt.MENU_POPUP)
		for label, column in (("Sort by Name", TorRow.COL_NAME),
			("Sort by Size", TorRow.COL_SIZE),
			("Sort by Progress", TorRow.COL_PROG),
			("Sort by Download Speed", TorRow.COL_DS),
			("Sort by ETA", TorRow.COL_ETA),
			):
			item = gnt.MenuItemCheck(label)
			if TorRow.compare_column == column:
				item.set_checked(True)
			item.connect('activate', sort_col_cb, column)
			menu.add_item(item)
		gnt.show_menu(menu)

gobject.type_register(TorList)
gnt.register_bindings(TorList)

def setup_fileselector(fl, win):
	def close_filesel(cancel, window):
		window.destroy()
	fl.cancel_button().connect('activate', close_filesel, win)


def show_details(tid):
	details = TorDetails.new(tid)
	details.show()
	details.present()

def setup_deluge_core():
	"""Do the foreplay with the deluge daemon."""
	client.set_core_uri("http://localhost:58846")

	local.configs_init()
	client.force_call()

def create_checkbox(label, config, configs = local.configs, reverse = False):
	check = gnt.CheckBox(label)
	value = configs[config]
	if reverse:
		value = True - value
	check.set_checked(value)
	check.set_data("config", config)
	check.set_data("config-value", value)
	check.get_value = check.get_checked
	return check

def create_combo(options, config, configs = local.configs):
	combo = gnt.ComboBox()
	iter = 0
	for option in options:
		combo.add_data(str(iter), option)
		iter = iter + 1
	value = configs[config]
	combo.set_data("config", config)
	combo.set_data("config-value", str(value))
	combo.set_selected(str(value))
	combo.get_value = combo.get_selected_data
	return combo

def setup_standard_save_callback(save, window, prefs):
	def save_callback(vt, data):
		window = data[0]
		prefs = data[1]
		newconfigs = {}
		for pref in prefs:
			oldv = pref.get_data("config-value")
			config = pref.get_data("config")
			val = pref.get_value()
			if val != oldv:
				newconfigs[config] = refine_config_value(config, val)
		try:
			client.set_config(newconfigs)
			window.destroy()
		except Exception, exc:
			log.error(str(exc))
	save.connect('activate', save_callback, (window, prefs))

def refine_config_value(config, value):
	"""Return the value of the configuration in correct type."""
	config_convert = {
		"max_connections_global" : int,
		"max_download_speed" : float,
		"max_upload_speed" : float,
		"max_upload_slots_global" : int,
		"max_connections_per_torrent" : int,
		"max_upload_slots_per_torrent" : int,
		"listen_ports" : int,
		"enc_in_policy" : int,
		"enc_out_policy" : int,
		"enc_level" : int,
	}
	if config in config_convert:
		conv = config_convert[config]
		if isinstance(value, list):
			for iter in range(0, len(value)):
				value[iter] = conv(value[iter])
		else:
			value = conv(value)
	return value

def pref_window(title):
	win = gnt.Window()
	win.set_property('vertical', True)
	win.set_title(title)
	win.set_alignment(gnt.ALIGN_MID)
	win.set_pad(0)
	save = gnt.Button("Save")
	cancel = gnt.Button("Cancel")
	def close_window(b, window):
		window.destroy()
	cancel.connect('activate', close_window, win)
	return win, save, cancel

def add_section(win, title):
	win.add_widget(bold_label_and_flag(title, gnt.TEXT_FLAG_BOLD | gnt.TEXT_FLAG_UNDERLINE))

def show_dl_preferences(item):
	win, save, cancel = pref_window("Download Preference")
	add_section(win, "Torrents")

	configs = local.configs

	full = create_checkbox("Do not allocate space for new files on creation.", 'compact_allocation', configs)
	prt = create_checkbox("Prioritize first and last pieces of files in torrent.", 'prioritize_first_last_pieces', configs)

	win.add_widget(full)
	win.add_widget(prt)
	win.add_widget(gnt.Line(False))
	win.add_widget(common.pack_widget(False, [save, cancel]))
	setup_standard_save_callback(save, win, (full, prt))
	win.show()

def show_bandwidth_pref(item):
	"""Bandwidth settings."""
	win, save, cancel = pref_window("Bandwidth Preference")

	configs = local.configs
	entries = []
	def hook_entry_with_config(entry, config):
		value = str(configs[config])
		entry.set_data("config", config)
		entry.set_data("config-value", value)
		entry.set_text(value)
		entry.get_value = entry.get_text
		entries.append(entry)

	win.add_widget(bold_label_and_flag("Global Bandwidth Usage", gnt.TEXT_FLAG_BOLD | gnt.TEXT_FLAG_UNDERLINE))
	maxcon = gnt.Entry("")
	maxdlsp = gnt.Entry("")
	maxupsp = gnt.Entry("")
	maxupsl = gnt.Entry("")

	for entry, config in ((maxcon, "max_connections_global"),
		                  (maxdlsp, "max_download_speed"),
		                  (maxupsp, "max_upload_speed"),
		                  (maxupsl, "max_upload_slots_global"),
			):
		hook_entry_with_config(entry, config)

	for label, entry in (("Maximum Connections:", maxcon),
		                 ("Maximum Download Speed (KB/s):", maxdlsp),
		                 ("Maximum Upload Speed (KB/s):", maxupsp),
		                 ("Maximum Upload Slots:", maxupsl),
			 ):
		win.add_widget(common.pack_widget(False, [gnt.Label(label), entry]))

	win.add_widget(bold_label_and_flag("Per Torrent Bandwidth Usage", gnt.TEXT_FLAG_BOLD | gnt.TEXT_FLAG_UNDERLINE))
	maxconpt = gnt.Entry("")
	maxupslpt = gnt.Entry("")
	for entry, config in ((maxconpt, "max_connections_per_torrent"),
		                  (maxupslpt, "max_upload_slots_per_torrent"),
			):
		hook_entry_with_config(entry, config)

	for label, entry in (("Maximum Connections:", maxconpt),
		                 ("Maximum Upload Slots:", maxupslpt),
			 ):
		win.add_widget(common.pack_widget(False, [gnt.Label(label), entry]))

	win.add_widget(gnt.Line(False))
	win.add_widget(common.pack_widget(False, [save, cancel]))
	setup_standard_save_callback(save, win, entries)
	win.show()

def show_network_pref(item):
	"""Network settings."""
	win, save, cancel = pref_window("Network Preference")

	configs = local.configs

	add_section(win, "Ports")
	randport = create_checkbox("Use Random Ports", "random_port", configs)
	def _got_port(port):
		win.add_widget(common.pack_widget(False, [randport, gnt.Label("   (Active Port: " + str(port) + ")")]))
	client.get_listen_port(_got_port)
	client.force_call()

	add_section(win, "DHT")
	dht = create_checkbox("Enable Mainline DHT", "dht", configs)
	win.add_widget(dht)

	add_section(win, "Network Extras")
	upnp = create_checkbox("UPnP", "upnp", configs)
	nat = create_checkbox("NAT-PMP", "natpmp", configs)
	pex = create_checkbox("uTorrent-PeX", "utpex", configs)
	win.add_widget(common.pack_widget(False, [upnp, nat, pex]))

	add_section(win, "Encryption")
	inb = create_combo(("Forced", "Enabled", "Disabled"), 'enc_in_policy', configs)
	outb = create_combo(("Forced", "Enabled", "Disabled"), 'enc_out_policy', configs)
	enclevel = create_combo(("Handshake", "Full Stream", "Either"), 'enc_level', configs)
	encpref = create_checkbox("Try to encrypt entire stream", 'enc_prefer_rc4', configs)

	win.add_widget(common.pack_widget(False, [gnt.Label("Inbound"), inb, gnt.Label("Outbound"), outb]))
	win.add_widget(common.pack_widget(False, [gnt.Label("Level"), enclevel, encpref]))

	win.add_widget(gnt.Line(False))
	win.add_widget(common.pack_widget(False, [save, cancel]))
	setup_standard_save_callback(save, win, (randport, dht, upnp, nat, pex, inb, outb, enclevel, encpref))
	win.show()

def create_menu():
	"""Create the menu for the main window."""
	menu = gnt.Menu(gnt.MENU_TOPLEVEL)

	file = gnt.MenuItem("File")
	menu.add_item(file)

	filesub = gnt.Menu(gnt.MENU_POPUP)
	file.set_submenu(filesub)

	qt = gnt.MenuItem("Quit")
	def qt_cb(q):
		def _quit():
			delugent_terminate()
		gobject.timeout_add_seconds(0, _quit)
	qt.connect('activate', qt_cb)
	filesub.add_item(qt)

	edit = gnt.MenuItem("Edit")
	menu.add_item(edit)

	editsub = gnt.Menu(gnt.MENU_POPUP)
	edit.set_submenu(editsub)

	pref = gnt.MenuItem("Preference")
	prefsub = gnt.Menu(gnt.MENU_POPUP)
	pref.set_submenu(prefsub)
	editsub.add_item(pref)

	for name, cb in (("Downloads", show_dl_preferences),
		             ("Network", show_network_pref),
		             ("Bandwidth", show_bandwidth_pref),
		             #("Other", dummy),
		             #("Plugins", dummy),
		            ):
		dl = gnt.MenuItem(name)
		dl.connect('activate', cb)
		prefsub.add_item(dl)

	conn = gnt.MenuItem("Connections")
	editsub.add_item(conn)

	return menu

def select_files_from_torrent(filename):
	win = gnt.Window()
	win.set_property('vertical', True)
	win.set_title("Select Files & Options ...")
	win.set_alignment(gnt.ALIGN_MID)
	win.set_pad(0)
	win.set_maximize(gnt.WINDOW_MAXIMIZE_Y)

	lbox = gnt.Box(False, False)
	lbox.set_pad(0)
	lbox.set_alignment(gnt.ALIGN_TOP)

	box = gnt.Box(False, True)
	box.set_pad(0)

	box.add_widget(gnt.Label("Unselect the files you don't want to download."))
	box.add_widget(gnt.Line(False))

	# List of files in the torrent
	t = TorFileSelector(filename)
	t.populate()
	gnt.set_flag(t, gnt.WIDGET_NO_BORDER | gnt.WIDGET_NO_SHADOW)
	box.add_widget(t)

	box.add_widget(gnt.Line(False))

	# Some options to override the defaults
	add_paused = create_checkbox("Do not start downloading yet", 'add_paused', local.configs)
	box.add_widget(add_paused)

	full_alloc = create_checkbox("Allocate space for the selected files immediately", 'compact_allocation', local.configs, True)
	box.add_widget(full_alloc)

	def change_dl_loc(b, label):
		def location_selected(fl, path, file, label):
			fl.destroy()
			client.set_config({'download_location': path})
			label.set_text("Download to: " + path)
		fl = gnt.FileSel()
		fl.set_title("Select Location")
		fl.set_dirs_only(True)
		fl.set_current_location(local.configs['download_location'])
		setup_fileselector(fl, fl)
		fl.connect('file_selected', location_selected, label)
		fl.show()
	downloadto = bold_label_and_flag("Download to: " + local.configs['download_location'], gnt.TEXT_FLAG_BOLD)
	change = gnt.Button("...")
	change.connect('activate', change_dl_loc, downloadto)
	sbox = common.pack_widget(False, [downloadto, change])
	sbox.set_fill(False)
	sbox.set_alignment(gnt.ALIGN_MID)
	box.add_widget(sbox)

	sbox = common.pack_widget(False, [
			common.pack_widget(True, [
				common.pack_widget(False, [common.fixed_label("Upload Speed  "), gnt.Entry("")]),
				common.pack_widget(False, [common.fixed_label("Download Speed"), gnt.Entry("")]),
			]), common.pack_widget(True, [
				common.pack_widget(False, [common.fixed_label("Upload Slots"), gnt.Entry("")]),
				common.pack_widget(False, [common.fixed_label("Connections "), gnt.Entry("")]),
			])
		])
	box.add_widget(sbox)

	lbox.add_widget(box)

	# Add options to the window
	box = gnt.Box(False, True)
	box.add_widget(gnt.Label("Maximum speed"))

	box.add_widget(common.pack_widget(False, [gnt.Label("Upload"), gnt.Entry("")]))
	box.add_widget(common.pack_widget(False, [gnt.Label("Download"), gnt.Entry("")]))

	#lbox.add_widget(box)

	win.add_widget(lbox)

	win.add_widget(gnt.Line(False))

	# OK/Cancel buttons
	ok = gnt.Button("Add")
	def _ok_add_torrent(button, selector):
		selector.accept({
				'add_paused' : add_paused.get_checked(),
				'compact_allocation' : True - full_alloc.get_checked(),
				# other options are: compact_allocation, max_download_speed_per_torrent,
				# max_upload_speed_per_torrent, max_connections_per_torrent,
				# max_upload_slots_per_torrent, prioritize_first_last_pieces,
				# add_paused, default_private
			})
		win.destroy()
	ok.connect('activate', _ok_add_torrent, t)

	cancel = gnt.Button("Cancel")
	def _cancel_add_torrent(button, selector):
		win.destroy()
	cancel.connect('activate', _cancel_add_torrent, t)

	win.add_widget(common.pack_widget(False, [ok, cancel]))

	win.show()

def main():
	try:
		setup_deluge_core()
	except Exception, msg:
		delugent_terminate()
		print "Error:", msg
		return

	win = gnt.Window()
	win.set_property('vertical', True)
	win.set_toplevel(True)
	win.set_title("Delugent")
	win.set_pad(0)
	win.set_alignment(gnt.ALIGN_MID)

	list = TorList()
	list.enable_sort()

	win.add_widget(list)
	width, height = gnt.screen_size()
	list.set_size(width, height)

	hbox = gnt.Box(homo = False, vert = False)

	add = gnt.Button("Add")
	def add_clicked(b):
		def file_selected(widget, path, file, window):
			files = widget.get_selected_multi_files()
			window.destroy()
			path = os.path.dirname(files[0])
			local.gntui['load_torrent_location'] = path
			for file in files:
				select_files_from_torrent(file)

		win = gnt.Window()
		win.set_property('vertical', True)
		win.set_pad(0)

		fl = gnt.FileSel()
		win.set_title("Select a Torrent")
		fl.set_multi_select(True)
		fl.set_current_location(local.gntui['load_torrent_location'])
		fl.connect("file_selected", file_selected, win)
		setup_fileselector(fl, win)
		gnt.set_flag(fl, gnt.WIDGET_NO_BORDER | gnt.WIDGET_NO_SHADOW)
		fl.draw()

		win.add_widget(fl)
		win.give_focus_to_child(fl)
		win.show()

	add.connect('activate', add_clicked)
	hbox.add_widget(add)
	gnt.gnt_util_set_trigger_widget(win, gnt.KEY_INS, add)

	rm = gnt.Button("Remove")
	def rm_clicked(b):
		tor = list.get_selection_data()
		client.remove_torrent([tor.id])
	rm.connect('activate', rm_clicked)
	hbox.add_widget(rm)
	gnt.gnt_util_set_trigger_widget(win, gnt.KEY_DEL, rm)

	hbox.add_widget(gnt.Label("|"))

	pause = gnt.Button("Pause")
	def pause_clicked(b):
		tor = list.get_selection_data()
		client.pause_torrent([tor.id])
	pause.connect('activate', pause_clicked)
	hbox.add_widget(pause)
	gnt.gnt_util_set_trigger_widget(win, 'p', pause)

	resume = gnt.Button("Resume")
	def resume_clicked(b):
		tor = list.get_selection_data()
		client.resume_torrent([tor.id])
	resume.connect('activate', resume_clicked)
	hbox.add_widget(resume)
	gnt.gnt_util_set_trigger_widget(win, 'r', resume)

	hbox.add_widget(gnt.Label("|"))

	info = gnt.Button("Details")
	def info_clicked(b):
		list.perform_action_named('info')
	info.connect('activate', info_clicked)
	hbox.add_widget(info)

	win.add_widget(hbox)

	menu = create_menu()   # XXX: Investigate why this causes warnings on exit
	win.set_menu(menu)

	win.show()

	signalhandler = signals.Signals(list)

	def update_rows(list):
		list.update_all()
		return True
	id = gobject.timeout_add_seconds(1, update_rows, list)
	def remove_update_timeout(list, id):
		gobject.source_remove(id)
	list.connect('destroy', remove_update_timeout, id)

	# Add the torrents in the list
	def got_session_state(session_state):
		for torrent_id in session_state:
			list.add_torrent(torrent_id)
	client.get_session_state(got_session_state)

	gnt.gnt_main()
	delugent_terminate()

def delugent_terminate():
	local.configs_uninit()
	gnt.gnt_quit()
	sys.exit(0)

if __name__ == "__main__":
	profile.run('main()', 'profile')
	#main()
