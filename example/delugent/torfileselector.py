from deluge.ui.client import aclient as client
import os.path

import gnt
import gobject
import common
import local

class TorFileSelectorRow(gobject.GObject):
	def __init__(self, file, path = None):
		self.__gobject_init__()
		self.file = file
		self.path = path

	def display(self):
		if self.file is not None:
			return [os.path.basename(self.file['path']), common.show_size(self.file['size'])]
		else:
			return [self.path, '']

	def __del__(self):
		pass

gobject.type_register(TorFileSelectorRow)

class TorFileSelector(gnt.Tree):
	def __init__(self, torrent):
		gnt.Tree.__init__(self)
		self.set_property('columns', 2)
		self.set_property('expander-level', 1)
		self.set_show_title(False)
		self.set_show_separator(False)
		self.torrent = torrent
		self.torfiles = common.torrent_info(torrent)
		self.set_column_is_right_aligned(1, True)
		self.set_col_width(1, 4)

	def accept(self, options):
		dl = []
		for f in self.get_rows():
			if self.get_choice(f):
				dl.append(1)
			else:
				dl.append(0)
		options['file_priorities'] = dl
		client.add_torrent_file([self.torrent], [options])
		client.force_call()

	def populate(self):
		parents = {}
		for file in self.torfiles:
			path = os.path.dirname(file['path'])
			if path in parents or len(path) == 0:
				continue
			parents[path] = f = TorFileSelectorRow(None, path)
			self.add_row_after(f, f.display(), None, None)
			self.set_row_flags(f, gnt.TEXT_FLAG_BOLD)

		for file in self.torfiles:
			f = TorFileSelectorRow(file)
			parent = None
			path = os.path.dirname(file['path'])
			if path in parents:
				parent = parents[path]
			self.add_row_after(f, f.display(), parent, None, True)
			self.set_choice(f, True)

gobject.type_register(TorFileSelector)
gnt.register_bindings(TorFileSelector)

