from deluge.ui.client import aclient as client
import os.path

import gnt
import gobject
import common
import local

_tor_settings = {}

class TorSettings(gnt.Window):
	def new(cls, torrent_id):
		if torrent_id in _tor_settings:
			return _tor_settings[torrent_id]
		r = cls(torrent_id)
		_tor_settings[torrent_id] = r
		return r
	new = classmethod(new)

	def __init__(self, torrent_id):
		gnt.Window.__init__(self)
		self.torrent = torrent_id
		self.set_title("Change Torrent Settings...")
		self.set_property('vertical', True)
		self.set_pad(0)
		self.set_alignment(gnt.ALIGN_MID)

		self.label = gnt.Label("...")
		self.add_widget(self.label)
		self.add_widget(gnt.Line(False))

		self.maxdls = gnt.Entry("")
		self.maxups = gnt.Entry("")
		self.maxcon = gnt.Entry("")
		self.priv = gnt.CheckBox("Private")
		self.maxdls.set_size(8, 1)
		self.maxups.set_size(8, 1)
		self.maxcon.set_size(8, 1)

		for label, widget in (
			('Maximum Download Speed (KB/s)', self.maxdls),
			('Maximum Upload Speed (KB/s)', self.maxups),
			('Maximum Connections', self.maxcon)):
			self.add_widget(common.pack_widget(False, [gnt.Label(label), widget]))
		self.add_widget(self.priv)

		save = gnt.Button("Save")
		cancel = gnt.Button("Cancel")
		self.add_widget(common.pack_widget(False, [save, cancel]))

		def _on_save(s):
			for widget, fn in (
				(self.maxdls, client.set_torrent_max_download_speed),
				(self.maxups, client.set_torrent_max_upload_speed),
				(self.maxcon, client.set_torrent_max_connections),
			):
				nvalue = widget.get_text()
				ovalue = widget.get_data('original-value')
				if nvalue != ovalue:
					fn(self.torrent, float(nvalue))
			if self.priv.get_checked() != self.priv.get_data('original-value'):
				client.set_torrent_private_flag(self.torrent, self.priv.get_checked())
			self.destroy()
		save.connect('activate', _on_save)

		def _on_cancel(c):
			self.destroy()
		cancel.connect('activate', _on_cancel)

		def _got_details(status):
			for key, widget in (
					('max_download_speed', self.maxdls),
					('max_upload_speed', self.maxups),
					('max_connections', self.maxcon),
			):
				value = str(status[key])
				widget.set_text(value)
				widget.set_data('original-value', value)
			self.priv.set_checked(int(status['private']))
			self.priv.set_data('original-value', int(status['private']))
			self.label.set_text(str(status['name']))

		client.get_torrent_status(_got_details, self.torrent,
			['max_connections', 'max_upload_slots', 'max_upload_speed', 'max_download_speed', 'private', 'name'])
		client.force_call()

		def remove_self(self):
			del _tor_settings[self.torrent]
		self.connect('destroy', remove_self)
gobject.type_register(TorSettings)

