import deluge.common
import gnt

def torrent_info(filename):
	# (from deluge)
	# This feels really ugly
	import deluge.libtorrent as lt
	f = open(filename, "rb")
	if not f:
		return None
	fdump = lt.bdecode(f.read())
	f.close()
	info = lt.torrent_info(fdump)
	files = []
	for f in info.files():
		files.append({
			'path' : f.path,
			'size' : f.size,
			'download' : True,
		})
	return files

def show_size(size):
	unit = "b"
	if size > (1 << 30):
		size = size >> 30
		unit = "G"
	elif size > (1 << 20):
		size = size >> 20
		unit = "M"
	elif size > (1 << 10):
		size = size >> 10
		unit = "K"
	return str(size) + unit

def show_speed(speed):
	unit = "b"
	if speed > (1 << 30):
		speed = speed / (1 << 30)
		unit = "G"
	elif speed > (1 << 20):
		speed = speed / (1 << 20)
		unit = "M"
	elif speed > (1 << 10):
		speed = speed / (1 << 10)
		unit = "K"
	return ("%.01f" + unit) % speed

def pack_widget(vert, widgets):
	box = gnt.Box(vert = vert, homo = False)
	box.set_fill(0)
	box.set_alignment(gnt.ALIGN_MID)
	for widget in widgets:
		box.add_widget(widget)
	return box

def fixed_label(text):
	label = gnt.Label(text)
	gnt.unset_flag(label, gnt.WIDGET_GROW_X)
	return label

