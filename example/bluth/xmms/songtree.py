#!/usr/bin/env python

"""
Song Tree.
"""

import gnt
import os
import sys
import treerow
import songedit
import xmmsclient.collections as xc

def safe_id3_attr(id3, attr):
	try:
		return id3[attr]
	except:
		return "---"

def get_id3_info(songtree, path, names):
	for name in names:
		filename = path + "/" + name
		if not os.path.isfile(filename):
			continue
		id3 = None
		try:
			id3 = ID3.ID3(filename)
			art = safe_id3_attr(id3, 'ARTIST')
			if art not in songtree.artists:
				songtree.artists[art] = []
			songtree.artists[art].append([id3, filename])
		except:
			pass

def sanitize(string):
	try:
		return unicode(string)
	except:
		try:
			return unicode(string, 'iso-8859-1')
		except:
			return string

class SongTree:
	def __init__(self, xmms):
		self.xmms = xmms
		self.win = None
		self.artistlist = None
		self.songlist = None
		self.artists = {}
		self.connect_to_xmms_events()
		self.mlib = {}

	def connect_to_xmms_events(self):
		self.xmms.broadcast_medialib_entry_changed(self.request_song_information)
		self.xmms.broadcast_medialib_entry_added(self.request_song_information)

	def request_song_information(self, id):
		if not isinstance(id, long):
			id = id.value()
		self.xmms.medialib_get_info(id, self.got_song_information)

	def mlib_entry_changed_cb(self, res):
		update_song_edits(self.songlist, None, None, songtree)
		pass

	def got_song_information(self, res):
		song = res.value()
		art = None
		try:
			art = song['artist']
		except:
			#return
			art = "----"
		if art not in self.artists:
			self.artists[art] = []
			if self.artistlist:
				row = treerow.Row()
				row.set_data('artist', art)
				self.artistlist.add_row_after(row, [str(sanitize(str(art)))], None)
		if song['id'] in self.mlib:
			s = self.mlib[song['id']]
			self.artists[art].remove(s)
			if self.get_selected_song() == s:
				self.songedit.set_info(song)
		self.mlib[song['id']] = song
		self.artists[art].append(song)

	def get_selected_song(self):
		sel = self.songlist.get_selection_data()
		if not sel:
			return None
		song = sel.get_data('song')
		return song

	def collection_info(self, result):
		songs = result.value()
		for song in songs:
			self.request_song_information(song)

	def scan(self):
		uni = xc.Universe()
		self.xmms.coll_query_ids(uni, 0, 0, None, self.collection_info)

	def show(self):
		win = gnt.Window()
		win.set_title("XMMS2 Medialib Browser")

		hbox = gnt.Box(homo = False, vert = False)
		win.add_widget(hbox)

		artree = gnt.Tree()
		artree.set_col_width(0, 30)
		artree.set_column_title(0, 'Artist')
		artree.set_show_title(True)

		songlist = gnt.Tree()
		songlist.set_col_width(0, 40)
		songlist.set_column_title(0, 'Song')
		songlist.set_show_title(True)

		vbox = gnt.Box(homo = False, vert = True)
		vbox.set_pad(0)
		hbox.add_widget(artree)
		hbox.add_widget(vbox)

		self.songedit = songedit.SongEdit(self.xmms)
		vbox.add_widget(self.songedit.get_widget())
		vbox.add_widget(songlist)

		self.win = win
		self.artistlist = artree
		self.songlist = songlist

		for (artist, songs) in self.artists.iteritems():
			row = treerow.Row()
			row.set_data('artist', artist)
			artree.add_row_after(row, [str(artist)], None)

		artree.connect('selection-changed', update_song_list, self)
		artree.connect('key-pressed', artistlist_key_pressed, self)
		songlist.connect('selection-changed', update_song_edits, self)
		songlist.connect('key-pressed', songlist_key_pressed, self)

		win.show()
	
	def remove_artist(self, rart):
		rows = self.artistlist.get_rows()
		for row in rows:
			artist = row.get_data('artist')
			if artist == rart:
				self.artistlist.remove(row)
				break

def update_song_edits(tree, old, new, songtree):
	song = songtree.get_selected_song()
	songtree.songedit.set_info(song)

def update_song_list(tree, old, new, songtree):
	songtree.songlist.remove_all()
	sel = songtree.artistlist.get_selection_data()
	artist = ""
	try:
		artist = sel.get_data('artist')
	except:
		return
	songs = songtree.artists[artist]
	for song in songs:
		row = treerow.Row()
		row.set_data('song', song)
		row.set_data('artist', artist)
		title = "..."
		if 'title' in song:
			title = song['title']
		songtree.songlist.add_row_after(row, [str(title)], None)
	update_song_edits(songtree.songlist, None, None, songtree)

def artistlist_key_pressed(tree, key, songtree):
	if key == 't':
		return True
	return False

def songlist_key_pressed(tree, key, songtree):
	if key == gnt.KEY_DEL:
		row = songtree.songlist.get_selection_data()
		if not row: return False
		song = row.get_data('song')
		artist = row.get_data('artist')
		songtree.xmms.medialib_remove_entry(song['id'])
		songtree.artists[artist].remove(song)
		if len(songtree.artists[artist]) == 0:
			del songtree.artists[artist]
			songtree.remove_artist(artist)
		update_song_list(songtree.artistlist, None, None, songtree)
		return True
	elif tree.is_searching():
		return False
	elif key == 'E':
		songtree.win.give_focus_to_child(songtree.songedit.title)
	return False

