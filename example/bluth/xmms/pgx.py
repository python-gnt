#!/usr/bin/env python
#  XMMS2 - X Music Multiplexer System
#  Copyright (C) 2007 Sadrul Habib Chowdhury
# 
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License, or (at your option) any later version.
#                   
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.

import xmmsclient
import os
import sys
import xmmsclient.glib as xmmsglib
import gobject
import xmmsclient.collections as xc

import gnt
import songtree

xmms = None

num = 0

def got_song_information(result):
	res = result.value()
	print res['artist']

def request_song_information(id):
	xmms.medialib_get_info(id, got_song_information)

def my_current_id(result):
	global num
	num = num + 1
	entry.set_text(str(result.value()) + " " + str(num))
	request_song_information(result.value())

def collection_info(result):
	songs = result.value()
	for song in songs:
		print song
	pass
def setup_xmms():
	global xmms
	xmms = xmmsclient.XMMS("pygnt-test")
	try:
		xmms.connect(os.getenv("XMMS_PATH"))
		conn = xmmsglib.GLibConnector(xmms)
	except IOError, detail:
		print "Connection failed:", detail
		gnt.gnt_quit()
		return False
	"""
	xmms.broadcast_playback_current_id(my_current_id)
	xmms.playback_current_id(my_current_id)

	uni = xc.Universe()
	xmms.coll_query_ids(uni, 0, 0, None, collection_info)
	"""

	return False

setup_xmms()

"""
win = gnt.Box(False, False)
win.set_toplevel(True)

entry = gnt.Entry("...")
win.add_widget(entry)

songlist = gnt.Tree()
songlist.set_property('columns', 5)

win.show()
"""

tree = songtree.SongTree(xmms)
tree.scan()
tree.show()

gnt.gnt_main()

gnt.gnt_quit()

