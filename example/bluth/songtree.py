#!/usr/bin/env python

"""
Song Tree.
"""

import gnt
import os
import ID3
import treerow
import songedit

def safe_id3_attr(id3, attr):
	try:
		return id3[attr]
	except:
		return "---"

def get_id3_info(songtree, path, names):
	for name in names:
		filename = path + "/" + name
		if not os.path.isfile(filename):
			continue
		id3 = None
		try:
			id3 = ID3.ID3(filename)
			art = safe_id3_attr(id3, 'ARTIST')
			if art not in songtree.artists:
				songtree.artists[art] = []
			songtree.artists[art].append([id3, filename])
		except:
			pass

class SongTree:
	def __init__(self, path = None):
		self.path = path
		self.win = None
		self.artistlist = None
		self.songlist = None
		self.artists = {}

	def scan(self):
		os.path.walk(self.path, get_id3_info, self)

	def show(self):
		win = gnt.Window()
		win.set_title("Bluth")

		hbox = gnt.Box(homo = False, vert = False)
		win.add_widget(hbox)

		artree = gnt.Tree()
		artree.set_col_width(0, 30)
		artree.set_column_title(0, 'Artist')
		artree.set_show_title(True)

		songlist = gnt.Tree()
		songlist.set_col_width(0, 40)
		songlist.set_column_title(0, 'Song')
		songlist.set_show_title(True)

		vbox = gnt.Box(homo = False, vert = True)
		vbox.set_pad(0)
		hbox.add_widget(artree)
		hbox.add_widget(vbox)

		self.songedit = songedit.SongEdit()
		vbox.add_widget(self.songedit.get_widget())
		vbox.add_widget(songlist)

		self.win = win
		self.artistlist = artree
		self.songlist = songlist

		for (artist, songs) in self.artists.iteritems():
			row = treerow.Row()
			row.set_data('artist', artist)
			artree.add_row_after(row, [str(artist)], None)

		artree.connect('selection-changed', update_song_list, self)
		songlist.connect('selection-changed', update_song_edits, self)

		win.show()

def update_song_edits(tree, old, new, songtree):
	sel = tree.get_selection_data()
	song = {}
	song['id3'] = sel.get_data('song')
	song['file'] = sel.get_data('filename')
	songtree.songedit.set_info(song)

def update_song_list(tree, old, new, songtree):
	songtree.songlist.remove_all()
	sel = songtree.artistlist.get_selection_data()
	artist = ""
	try:
		artist = sel.get_data('artist')
	except:
		return
	songs = songtree.artists[artist]
	for song in songs:
		row = treerow.Row()
		row.set_data('song', song[0])
		row.set_data('filename', song[1])
		songtree.songlist.add_row_after(row, [safe_id3_attr(song[0], 'TITLE')], None)
	update_song_edits(songtree.songlist, None, None, songtree)

