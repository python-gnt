import gobject

class Row(gobject.GObject):
	def __init__(self):
		self.__gobject_init__()
		self.tagged = False

	def __del__(self):
		pass

gobject.type_register(Row)

