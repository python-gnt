#!/usr/bin/env python

from museek import messages

_settings = {}

def convert_size(bytes):
	unit = "B"
	up = (1 << 10)
	try:
		bytes = float(bytes)
	except:
		return "--"
	if bytes >= up:
		unit = "K"
		bytes = bytes / up
	if bytes >= up:
		unit = "M"
		bytes = bytes / up
	if bytes >= up:
		unit = "G"
		bytes = bytes / up
	return "%.2f%s" % (bytes, unit)

def convert_speed(speed):
	return convert_size(speed)  # XXX: for now

def convert_length(length):
	try:
		length = int(length)
	except:
		return "--"
	seconds = length % 60
	minutes = length / 60
	if minutes > 60:
		hours = minutes / 60
		minutes = minutes % 60
		return "%d:%02d:%02d" % (hours, minutes, seconds)
	else:
		return "%d:%02d" % (minutes, seconds)


def convert_name_from_path(path):
	name = path
	name = name[name.rfind('/') + 1:]
	name = name[name.rfind('\\') + 1:]
	return name

def convert_transfer_state(state):
	states = {
		messages.TS_Finished : "Finished",
		messages.TS_Transferring : "Transerring",
		messages.TS_Negotiating : "Negotiating",
		messages.TS_Waiting : "Waiting",
		messages.TS_Establishing : "Establishing",
		messages.TS_Initiating : "Initiating",
		messages.TS_Connecting : "Connecting",
		messages.TS_Queued : "Queued",
		messages.TS_Address : "Getting Address",
		messages.TS_Status : "Getting Status",
		messages.TS_Offline : "Offline",
		messages.TS_ConnectionClosed : "Closed",
		messages.TS_CannotConnect : "Cannot Connect",
		messages.TS_Aborted : "Aborted",
		messages.TS_Error : None,

	}
	if state in states:
		return states[state]
	return None

def init_settings(settings):
	global _settings
	_settings = settings

def settings():
	return _settings

