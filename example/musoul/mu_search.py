"""Search window for mu"""
import gnt
from museek import messages, driver
import gobject

import mu_common

# TODO:
#	- Add context menu to hide/show columns

_wins = {}

class SearchWindow(gnt.Window):
	"""An entire search window"""

	POSITION = 0
	NAME = 1
	SIZE = 2
	BITRATE = 3
	BANDWIDTH = 4
	LENGTH = 5
	QUEUE = 6
	USER = 7
	PATH = 8
	COLUMNS = 9

	def __init__(self, ticket, query):
		gnt.Window.__init__(self)
		self.ticket = ticket
		self.query = query
		self.set_title(self.query + " -- Results")
		self.set_pad(0)
		self.count = 0
		self.tooltip = None

		# Setup the widgets
		self.results = tree = gnt.Tree()
		tree.set_property('columns', self.COLUMNS)
		tree.set_show_title(True)
		tree.set_search_column(self.NAME)
		tree.set_show_separator(False)

		# the settings for each column
		for pos, title, right, size in (
					(self.POSITION, "#", True, 4),
					(self.NAME, "Name", False, 0),
					(self.SIZE, "Size", True, 7),
					(self.BANDWIDTH, "Speed", True, 7),
					(self.USER, "User", False, 8),
					(self.PATH, "Path", False, 0),
					(self.BITRATE, "Bitrate", True, 7),
					(self.LENGTH, "Length", True, 7),
					(self.QUEUE, "Q", True, 3),
				):
			tree.set_column_title(pos, title)
			tree.set_column_is_right_aligned(pos, right)
			if size > 0:
				tree.set_col_width(pos, size)
				tree.set_column_resizable(pos, False)
			else:
				tree.set_column_resizable(pos, True)

		self.add_widget(self.results)

		self.set_maximize(gnt.WINDOW_MAXIMIZE_Y | gnt.WINDOW_MAXIMIZE_X)
		self.show()

		self.connect('destroy', self.do_destroy)  # I don't really know why this is necessary

		def _start_download(tree):
			item = tree.get_selection_data()
			if item:
				mu_common.settings()['main'].download(item.user, item.path)
				tree.set_row_flags(item, gnt.TEXT_FLAG_BOLD)
		self.results.connect('activate', _start_download)

		def _selection_changed(tree, old, current):
			if self.tooltip:
				self.tooltip.destroy()
				self.tooltip = None

			current = tree.get_selection_data()
			if not current:
				return

			self.tooltip = gnt.Box(vert = False, homo = False)
			self.tooltip.set_title("Information")
			self.tooltip.set_toplevel(True)

			tv = gnt.TextView()
			tv.set_flag(gnt.TEXT_VIEW_NO_SCROLL | gnt.TEXT_VIEW_TOP_ALIGN)

			text = ""
			for label, item in (
					("Name", current.name),
					("Size", mu_common.convert_size(current.size)),
					("Bitrate", str(current.bitrate)),
					("Speed", mu_common.convert_speed(current.speed)),
					("Length", mu_common.convert_length(current.length))
					):
				if len(text) > 0:
					tv.append_text_with_flags("\n", 0)
				tv.append_text_with_flags(label + ": ", gnt.TEXT_FLAG_BOLD)
				tv.append_text_with_flags(item, gnt.TEXT_FLAG_NORMAL)
				text = "%s\n%s: %s" % (text, label, item)
			text = text.strip()
			twidth, theight = gnt.get_text_bound(text)
			tv.set_size(twidth, theight)

			tx, ty = tree.get_position()
			width, height = tree.get_size()
			x = tx + width
			y = ty + tree.get_selection_visible_line() + 4
			if y + theight + 3> gnt.screen_size()[1]:
				y = ty + 1 - theight + tree.get_selection_visible_line()
			self.tooltip.set_position(x, y)

			self.tooltip.add_widget(tv)
			gnt.unset_flag(self.tooltip, gnt.WIDGET_CAN_TAKE_FOCUS)
			gnt.set_flag(self.tooltip, gnt.WIDGET_TRANSIENT)
			self.tooltip.draw()

		self.results.connect('selection-changed', _selection_changed)

	def add_search_result(self, item):
		self.count = self.count + 1

		row = [""] * self.COLUMNS
		row[self.POSITION] = str(self.count)
		row[self.NAME] = item.name
		row[self.SIZE] = mu_common.convert_size(item.size)
		row[self.BITRATE] = str(item.bitrate)
		row[self.BANDWIDTH] = mu_common.convert_speed(item.speed)
		row[self.LENGTH] = mu_common.convert_length(item.length)
		row[self.PATH] = item.path
		row[self.USER] = item.user
		row[self.QUEUE] = str(item.queue)

		self.results.add_row_after(item, row, None)

		if not item.free:
			self.results.set_row_flags(item, gnt.TEXT_FLAG_DIM)

	def do_destroy(self, data):
		del _wins[self.ticket]

gobject.type_register(SearchWindow)

class SearchItem(gobject.GObject):
	"""A single result item"""
	def __init__(self, user, free, speed, queue, result):
		gobject.GObject.__init__(self)
		self.user = user
		self.free = free
		self.speed = speed
		self.queue = queue
		self.path = result[0]
		self.size = result[1]
		self.ext = result[2]
		try:
			self.bitrate = result[3][0]
			self.length = result[3][1]
		except:
			self.bitrate = "--"
			self.length = "--"

		self.name = mu_common.convert_name_from_path(self.path)
		self.downloading = False

gobject.type_register(SearchItem)

def search_result(ticket, user, free, speed, queue, results):
	if ticket not in _wins:
		return
	win = _wins[ticket]
	for res in results:
		item = SearchItem(user, free, speed, queue, res)
		win.add_search_result(item)

def new_search(ticket, query):
	if ticket in _wins:
		return
	_wins[ticket] = SearchWindow(ticket, query)

