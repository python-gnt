#!/usr/bin/env python

from museek import messages, driver
from mucipher import Cipher
import gobject
import gnt

import mu_search
import mu_download
import mu_common

def pack_widget(widgets, vert = False, homo = False):
	box = gnt.Box(vert = vert, homo = homo)
	box.set_fill(False)
	box.set_alignment(gnt.ALIGN_MID)
	for widget in widgets:
		if type(widget) == gnt.Label:
			gnt.unset_flag(widget, gnt.WIDGET_GROW_X)
		box.add_widget(widget)
	return box

class Net(driver.Driver):
	def __init__(self):
		self.password = None
		self.host = None
		self.connected = False
		pass
	
	def connect(self):
		self.cipher = Cipher(self.password)
		driver.Driver.connect(self, self.host, self.password, messages.EM_TRANSFERS)

	def disconnect(self):
		#message = messages.DisconnectServer()
		pass

	def download(self, user, path):
		if self.connected:
			message = messages.DownloadFile(user, path)
			self.send(message)

	# aborts a download, doesn't remove from list
	def download_abort(self, user, path):
		if self.connected:
			message = messages.TransferAbort(0, user, path)
			self.send(message)

	def download_remove(self, user, path):
		if self.connected:
			message = messages.TransferRemove(0, user, path)
			self.send(message)

	def cb_server_state(self, state, username):
		#print "Username:", username
		#print "State:", state
		#print "*****"
		pass

	def cb_login_ok(self):
		self.connected = True
		#message = messages.Search(0, "saul williams")
		#self.send(message)

	def cb_search_results(self, ticket, user, free, speed, queue, results):
		#print "Ticket:", ticket
		#print "User:", user
		#print "Free:", free
		#print "Speed:", speed
		#print "Queue:", queue
		#print "Results:"
		#for r in results:
		#	print "\t", r
		#print "***"
		mu_search.search_result(ticket, user, free, speed, queue, results)

	# called when a new search begins
	def cb_search_ticket(self, query, ticket):
		mu_search.new_search(ticket, query)

class Main(Net, gnt.Window):
	def __init__(self):
		gnt.Window.__init__(self)
		Net.__init__(self)

		self.win = win = gnt.Window()

		win.set_title("Mu - Soulseek Client")
		win.set_property('vertical', True)
		win.set_alignment(gnt.ALIGN_MID)
		win.set_pad(0)

		self.ent_name = gnt.Entry("")
		self.ent_pass = gnt.Entry("")
		self.ent_pass.set_masked(True)
		button = gnt.Button("Connect")
		self.but_connect = button

		win.add_widget(pack_widget([
					gnt.Label("Host"), self.ent_name,
					gnt.Label("Password"), self.ent_pass,
					self.but_connect
				]))
		button.connect('activate', self._connect)

		entry = gnt.Entry("");
		entry.set_history_length(-1)
		win.add_widget(pack_widget([gnt.Label("Search"), entry], False, False))
		self.ent_search = entry
		def _search(widget):
			text = widget.get_text()
			message = messages.Search(0, text)
			self.send(message)
			widget.clear()
			widget.add_to_history(text)
		entry.connect('activate', _search)

		button = gnt.Button("Disonnect")
		#win.add_widget(pack_widget([button], False, False))
		self.but_disconnect = button

		win.add_widget(gnt.Line(False))

		self.downloads = mu_download.DownloadList()
		win.add_widget(self.downloads)

		win.show()
		self.cb_disconnected()

	def cb_disconnected(self):
		self.connected = False
		for widget in (self.ent_search, self.but_disconnect):
			gnt.unset_flag(widget, gnt.WIDGET_CAN_TAKE_FOCUS)

		for widget in (self.ent_name, self.ent_pass, self.but_connect):
			gnt.set_flag(widget, gnt.WIDGET_CAN_TAKE_FOCUS)

	def cb_login_ok(self):
		self.connected = True
		for widget in (self.ent_search, self.but_disconnect):
			gnt.set_flag(widget, gnt.WIDGET_CAN_TAKE_FOCUS)

		for widget in (self.ent_name, self.ent_pass, self.but_connect):
			gnt.unset_flag(widget, gnt.WIDGET_CAN_TAKE_FOCUS)

	def cb_transfer_state(self, downloads, uploads):
		self.downloads.populate(downloads)

	def cb_transfer_update(self, transfer):
		if transfer.is_upload:
			pass
		else:
			self.downloads.add_item(transfer)

	def cb_transfer_remove(self, transfer):
		if transfer[0]:
			pass
		else:
			self.downloads.remove_item((transfer[1], transfer[2]))

	def _connect(self, null):
		self.host = self.ent_name.get_text()
		self.password = self.ent_pass.get_text()
		self.connect()
		def have_some_data(source, condition):
			self.process()
			return True
		gobject.io_add_watch(self.socket.fileno(), gobject.IO_IN, have_some_data)

gobject.type_register(Main)

main = Main()

settings = {'main': main}
mu_common.init_settings(settings)

gnt.gnt_main()
gnt.gnt_quit()

