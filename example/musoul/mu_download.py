#!/usr/bin/env python

import gnt
from museek import messages, driver
import gobject

import curses

import mu_common

class DownloadList(gnt.Tree):

	NAME = 0
	SIZE = 1
	DONE = 2
	SPEED = 3
	STATUS = 4
	QUEUE = 5
	USER = 6
	COLUMNS = 7

	COLOR_GOOD = gnt.gnt_color_add_pair(curses.COLOR_BLUE, -1)
	COLOR_BAD = gnt.gnt_color_add_pair(curses.COLOR_RED, -1)

	def __init__(self):
		gnt.Tree.__init__(self)
		self.hash = {}
		gnt.set_flag(self, gnt.WIDGET_NO_BORDER)

		tree = self
		tree.set_property('columns', self.COLUMNS)
		tree.set_show_title(True)
		tree.set_show_separator(False)

		for pos, title, right, size in (
					(self.NAME, "Name", False, 25),
					(self.SIZE, "Size", True, 7),
					(self.SPEED, "Speed", True, 7),
					(self.STATUS, "Status", False, 15),
					(self.DONE, "Done", True, 7),
					(self.QUEUE, " Q ", True, 3),
					(self.USER, "User", False, 8)					
				):
			tree.set_column_title(pos, title)
			tree.set_column_is_right_aligned(pos, right)
			if size > 0:
				tree.set_col_width(pos, size)
				tree.set_column_resizable(pos, False)
			else:
				tree.set_column_resizable(pos, True)
		tree.set_column_resizable(self.NAME, True)

		def _key_pressed_cb(tree, key):
			if key == gnt.KEY_DEL:
				item = tree.get_selection_data()
				if item.state != messages.TS_Finished:
					mu_common.settings()['main'].download_abort(item.user, item.path)
				mu_common.settings()['main'].download_remove(item.user, item.path)
				return True
			return False
		self.connect('key_pressed', _key_pressed_cb)

	def add_item(self, down):
		if (down.user, down.path) in self.hash:
			item = self.hash[(down.user, down.path)]
			item.update(down)
			nw = False
		else:
			item = DownloadItem(down)
			nw = True

		row = [""] * self.COLUMNS
		row[self.NAME] = item.name
		row[self.SIZE] = mu_common.convert_size(item.size)
		row[self.SPEED] = mu_common.convert_speed(item.speed)
		row[self.STATUS] = mu_common.convert_transfer_state(item.state)
		if row[self.STATUS] is None:
			row[self.STATUS] = item.error
		row[self.USER] = item.user
		if item.queue > (1 << 20):
			row[self.QUEUE] = ""
		else:
			row[self.QUEUE] = str(item.queue)
		row[self.DONE] = mu_common.convert_size(item.done)
		if nw:
			self.add_row_after(item, row, None)
			self.hash[(item.user, item.path)] = item
		else:
			for i in range(self.COLUMNS):
				self.change_text(item, i, row[i])

		if item.state in (messages.TS_Finished, messages.TS_Transferring):
			self.set_row_color(item, self.COLOR_GOOD)
		elif item.state in (messages.TS_Offline, messages.TS_ConnectionClosed,
				messages.TS_CannotConnect, messages.TS_Aborted, messages.TS_Error):
			self.set_row_color(item, self.COLOR_BAD)
		else:
			self.set_row_color(item, gnt.COLOR_NORMAL)

		if item.state == messages.TS_Finished:
			self.set_row_flags(item, gnt.TEXT_FLAG_BOLD)
	
	def populate(self, downs):
		self.remove_all()
		self.draw()
		self.hash = {}

		for down in downs:
			self.add_item(down)

	def remove_item(self, down):
		if down not in self.hash:
			return
		item = self.hash[down]
		self.remove(item)
		del self.hash[down]

gobject.type_register(DownloadList)

class DownloadItem(gobject.GObject):
	def __init__(self, transfer):
		gobject.GObject.__init__(self)
		self.update(transfer)

	def update(self, transfer):
		self.path = transfer.path
		self.size = transfer.filesize
		self.state = transfer.state
		self.speed = transfer.rate
		self.error = transfer.error
		self.user = transfer.user
		self.name = mu_common.convert_name_from_path(self.path)
		self.queue = transfer.place
		self.done = transfer.filepos

gobject.type_register(DownloadItem)

_dwin = None

def downloads(downs):
	global _dwin
	if _dwin is None:
		_dwin = DownloadWindow()
	
	_dwin.populate(downs)

def download_update(download):
	global _dwin
	if _dwin is None:
		_dwin = DownloadWindow()

	_dwin.add_item(download)

def download_remove(user_path):
	if _dwin is None:
		return
	
	_dwin.remove_item(user_path)

